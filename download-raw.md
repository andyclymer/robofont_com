{% comment %} SLIDESHOW: WINDOWS {% endcomment %}
{% include slideshows items=page.slideShows.intro %}

{% comment %} SPLASH 1 {% endcomment %}
<div class="center splash-subtitle">
The <span id='adjective'></span> UFO font editor.
</div>

{% comment %} DOWNLOAD & BUY {% endcomment %}
<div class="center">
    <a class="buy-button" href="http://static.typemytype.com/robofont/RoboFont.dmg">download</a>
    <a class="buy-button" href="https://sites.fastspring.com/typemytype/instant/robofont3">buy</a>
</div>

{::options parse_block_html="true" /}

{% comment %} COLUMNS 1: INTRO TEXT {% endcomment %}
<div class="row item">
<div class="col">
RoboFont is a UFO-based font editor for macOS.

Written from scratch in Python with scalability in mind, allowing full scripting access to objects and interface.
</div>
<div class="col">
A fully featured font editor with all the basic tools required for drawing and editing typefaces.

A platform for building your own tools and extensions, and much more…!
</div>
</div>

{% comment %} SPLASH 2 {% endcomment %}
<div class="center splash-subtitle">
The tools you choose influence your creative process.
</div>

{% comment %} COLUMNS 2: TECHNICAL SPECS + EDUCATIONAL {% endcomment %}
<div class="row item">
<div class="col">
## Technical specifications

- requires macOS {{ site.data.versions.minimumSystem }} or higher
- uses UFO3 as native font format
- supports Python {{ site.data.versions.python }} out-of-the box

[read more…][technical specification]
</div>
<div class="col">
## Educational licenses

Teachers can request a free 1-year trial license for their students, or subscribe to the Student License Service.

[read more…][Educational licensing]
</div>
</div>

<br/>

{% comment %} SLIDESHOW: EXTENSIONS {% endcomment %}
{% include slideshows items=page.slideShows.intro-extensions %}

{% comment %} COLUMNS 3: EXTENSIONS {% endcomment %}
<div class="row item">
<div class="col">
## Open-source extensions

Dozens of open-source extensions by multiple developers are available on GitHub and via Mechanic.

[read more…][Extensions]
</div>
<div class="col">
## Commercial Extensions

Several other extensions by certified developers are available for a fee in the Extension Store.

[read more…][Extension Store]
</div>
</div>

{::options parse_block_html="false" /}

[technical specification]: {{ site.baseurl }}/technical-specification
[Student licenses]: {{ site.baseurl }}/student-license
[Educational licensing]: http://education.robofont.com
[Contact]: {{ site.baseurl }}/contact
[License Agreement]: {{ site.baseurl }}/eula
[Version History]: {{ site.baseurl }}/version-history
[Extensions]: {{ site.baseurl }}/documentation/extensions/
[Extension Store]: http://extensionstore.robofont.com

<script type="text/javascript">
    var adjectives = [
        'missing',
        'heavy-duty',
        'poetic',
        'extensible',
        'avant-garde',
        'professional',
        'interstellar',
        'pythonic',
        'water-proof',
        'elegant',
        'industrial',
    ];
    window.onload = function() {
        document.getElementById("adjective").innerHTML = adjectives[Math.floor(Math.random() * adjectives.length)];
    }
</script>

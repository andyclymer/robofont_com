path = "path/to/image"

w, h = imageSize(path)

s = 200

shift = -8, -28

######

import os

size(s, s)

im = ImageObject()
im.radialGradient(
    (s, s),
    center=(s*.5, s*.5),
    radius0=s*.4,
    radius1=s*.5,
    color0=(1, 1, 1, 0),
    color1=(1, 1, 1, 1))

image(path, shift)
image(im, (0, 0))

f, e = os.path.splitext(path)

saveImage(f + "_" + e)

import os
from yaml import load, dump, Loader, Dumper

#-------------
# import data
#-------------

basePath = os.path.abspath('../')
dataPath = os.path.join(basePath, '_data')
yamlPath = os.path.join(dataPath, 'extensionsMechanic.yml')

with open(yamlPath, 'r') as f:
    data = load(f)

#-------------
# export data
#-------------

destPath = '/_code/mechanic2/_data'

for extension in data:

    D = {}
    D['extensionName'] = extension['name']
    D['repository']    = extension['url']
    D['extensionPath'] = '%s.roboFontExt' % extension['name'].replace(' ', '')
    D['description']   = extension['description']
    D['tags']          = extension['tags']

    if isinstance(extension['author'], list):
        D['developer']     = extension['author'][0]
    else:
        D['developer']     = extension['author']

    try:
        D['developerURL']  = 'http://%s.com' % extension['author'].replace(' ', '').lower()
    except:
        D['developerURL'] = None

    yamlFile = '%s.yml' % extension['name'].replace(' ', '')
    yamlFile = yamlFile[0].lower() + yamlFile[1:]
    yamlPath = os.path.join(destPath, yamlFile)

    with open(yamlPath, 'w') as f:
        output = dump(D, f)

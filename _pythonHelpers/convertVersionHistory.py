path = u"/Users/frederik/Documents/apps/roboFont/robofont/changeHistory.md"

f = open(path, "r")
data = f.read()
f.close()

formatters = {
    "h3" : ""
}

indent = -1
INDENT = "   "

for line in data.split("\n"):
    if not line:
        continue
    if "<ul>" in line:
        indent += 1
    elif "</ul>" in line:
        indent -= 1
    if "<h3>" in line:
        line = line.replace("<h3>", "").replace("</h3>", "")
        line += "\n%s" % ("-" * len(line))

    warningStyle = 'style="color:red"' in line
    line = line.replace(' style="color:red"', '').replace('style="color:red"', '')

    if "<h4>" in line:
        line = line.replace("<h4>", "").replace("</h4>", "")
        line = ".. class:: required\n\n%s%s" % (INDENT, line)

    line = line.replace("*", "\*")
    line = line.replace("<li>", "%s* " % (indent * INDENT)).replace("</li>", "")
    line = line.replace("<ul>", "").replace("</ul>", "")
    line = line.replace("<strong>", "**").replace("</strong>", "**")
    line = line.replace("<b>", "**").replace("</b>", "**")

    line = line.replace("<br />", "").replace("<br/>", "")

    line = line.replace("<code>", "`").replace("</code>", "`")
    line = line.replace("<p>", "").replace("</p>", "")
    print(line)
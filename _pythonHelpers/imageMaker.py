path = '/_code/robofont_com/images/extensions/using-extensions_custom-tool.png'

###
import os

w, h = imageSize(path)
size(w, h)

border = 100

left = False
right = True
bottom = False
top = False

c0 = 1, 1, 1, 0
c1 = 1, 1, 1, 1

image(path, (0, 0))

if bottom:
    im = ImageObject()
    im.linearGradient((w, h),
        point0=(0, 0),
        point1=(0, border),
        color0=c1,
        color1=c0)

    image(im, (0, 0))

if left:
    im = ImageObject()
    im.linearGradient((w, h),
        point0=(border, 0),
        point1=(0, 0),
        color0=c0,
        color1=c1)

    image(im, (0, 0))

if top:
    im = ImageObject()
    im.linearGradient((w, h),
        point0=(0, 0),
        point1=(0, border),
        color0=c0,
        color1=c1)

    image(im, (0, h-border*2))

if right:
    im = ImageObject()
    im.linearGradient((w, h),
        point0=(0, 0),
        point1=(border, 0),
        color0=c0,
        color1=c1)

    image(im, (w-border*2, 0))

f, e = os.path.splitext(path)
saveImage(f + "_" + e)


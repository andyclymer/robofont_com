from html.parser import HTMLParser
import os
import yaml
import json
from urllib.request import urlopen

print("start extension extractor")

# don't overwrite manual edits in author & description attrs!
overwriteEdits = False
verbose = True


# get *.yml paths relative to script
pyHelpersDir = os.getcwd()
baseDir = os.path.dirname(pyHelpersDir)
dataDir = os.path.join(baseDir, '_data')
pathExtensionsMechanic = os.path.join(dataDir, 'extensionsMechanic.yml')
pathExtensionStore = os.path.join(dataDir, 'extensionsStore.yml')


# create a subclass and override the handler methods
class MyHTMLParser(HTMLParser):

    def __init__(self, *args, **kwargs):
        HTMLParser.__init__(self, *args, **kwargs)
        self.items = []
        self.in_td = False
        self.currentItem = None

        self.currentKey = None

    def handle_starttag(self, tag, attrs):
        if tag == "tr":
            self.currentItem = {}
            self.items.append(self.currentItem)
        if tag == "td":
            self.in_td = True
            for k, v in attrs:
                if k == "class":
                    self.currentKey = v

        if self.in_td and tag == "a":
            for k, v in attrs:
                if k == "href":
                    self.currentItem["url"] = v

    def handle_endtag(self, tag):
        if tag == "tr":
            self.currentItem = None
        if tag == "td":
            self.in_td = False

    def handle_data(self, data):
        data = data.strip()
        if self.in_td and self.currentItem and data:
            self.currentItem[self.currentKey] = data


# compare with existing data
def checkWithSource(item, sourceData):
    for sourceItem in sourceData:
        if sourceItem["name"] == item["name"]:
            # keep existing tags
            tags = sourceItem["tags"]
            if tags is None:
                tags = []
            if not isinstance(tags, list):
                tags = [tags]
            item["tags"] = tags
            # keep existing RF3
            RF3 = sourceItem.get("RF3")
            if RF3 is None:
                RF3 = ""
            item["RF3"] = RF3

            if not overwriteEdits:
                # keep edited description
                description = sourceItem["description"]
                if item["description"] != description:
                    if verbose:
                        print('skipping description in %s:' % item['name'])
                        print('\toriginal : %s' % item["description"])
                        print('\tedited   : %s' % description)
                        print()
                    item["description"] = description
                # keep edited author name
                author = sourceItem["author"]
                if item["author"] != author:
                    if verbose:
                        print('skipping author in %s:' % item['name'])
                        print('\toriginal : %s' % item["author"])
                        print('\tedited   : %s' % author)
                        print()
                    item["author"] = author
                    
            break


# extract data from mechanic website
response = urlopen('http://www.robofontmechanic.com')
mechanicHtml = response.read().decode("utf-8")

parser = MyHTMLParser()
parser.feed(mechanicHtml)

itemsMechanic = parser.items
itemsMechanic.remove({})

with open(pathExtensionsMechanic, encoding="utf-8") as f:
    sourceMechanic = yaml.load(f)


yamlOutput = ""
yamlFormatting = """- name: "%(name)s"
  author: %(author)s
  description: "%(description)s"
  url: %(url)s
  tags: %(tags)s
  RF3: %(RF3)s
  store: %(store)s
"""

tagFormatting = "\n    - %s"

for item in itemsMechanic:
    item["tags"] = []
    item["RF3"] = ""
    if "description" not in item:
        item["description"] = ""
    item["description"] = item["description"].replace("\"", "'")

    checkWithSource(item, sourceMechanic)

    item["tags"] = "".join([tagFormatting % tag for tag in item["tags"]])
    item["store"] = 'Mechanic'
    yamlOutput += yamlFormatting % item

with open(pathExtensionsMechanic, "w", encoding="utf-8") as f:
    f.write(yamlOutput)


# extension store data
response = urlopen('http://extensionstore.robofont.com/data.json')
extensionStoreJsonData = response.read().decode("utf-8")
extensionStoreData = json.loads(extensionStoreJsonData)

with open(pathExtensionStore) as f:
    sourceExtensionStore = yaml.load(f)

yamlOutput = ""

for item in extensionStoreData["extensions"]:
    item["author"] = item["developer"]
    item["url"] = item["link"]
    item["RF3"] = ""
    if "description" not in item:
        item["description"] = ""
    item["description"] = item["description"].replace("\"", "'")

    checkWithSource(item, sourceExtensionStore)

    item["tags"] = "".join([tagFormatting % tag for tag in item["tags"]])
    item["store"] = "Extension Store"
    yamlOutput += yamlFormatting % item
    
with open(pathExtensionStore, "w") as f:
    f.write(yamlOutput)

print("done extension extractor")
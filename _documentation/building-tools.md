---
layout: page
title: Building Tools
tags:
tree:
  - python
  - toolkit
  - toolspace
  - extensions
level: intermediate
---

**Everything you need to know to develop your own RoboFont-based tools.**

## Introduction to Python

*To build your own tools in RoboFont, you’ll need to learn Python first.*

* {% internallink "building-tools/python" %}

## The Python toolkit in RoboFont

*The building blocks for creating your own tools.*

{% tree "/documentation/building-tools/toolkit" levels=1 %}

## Exploring the toolspace

*A collection of examples showing common patterns and techniques.*

{% tree "/documentation/building-tools/toolspace" levels=1 %}

## Building extensions

*Packaging and distributing your RoboFont tools.*

{% tree "/documentation/building-tools/extensions" levels=1 %}

> - {% internallink "recommendations-upgrading-RF3" %}
{: .seealso }

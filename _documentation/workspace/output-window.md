---
layout: page
title: Output Window
tags:
  - scripting
level: beginner
---

The Output Window catches all print statements and tracebacks of scripts which are not running in the {% internallink "scripting-window" %}. For example: output from tools with dialogs or observers, RoboFont’s own warnings and error messages, etc.

{% image workspace/output-window.png %}

## Options

<table>
  <tr>
    <th width="25%">option</th>
    <th width="75%">description</th>
  </tr>
  <tr>
    <td>Can hide</td>
    <td>Enable this option to hide the window when RoboFont is not the active app (the Output Window acts like a floating window). When this option is disabled, the window will remain visible when RoboFont is not active.</td>
  </tr>
  <tr>
    <td>Clear</td>
    <td>Clears all print statements and tracebacks from the Output Window.</td>
  </tr>
</table>

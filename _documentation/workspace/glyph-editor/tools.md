---
layout: page
title: Tools
tree:
  - editing-tool
  - bezier-tool
  - slice-tool
  - measurement-tool
treeCanHide: true
level: beginner
---

{% image workspace/glyph-editor_tools.png %}

{% tree page.url levels=1 %}

## Glyph view events

These events are valid for all interactive tools.

<table>
  <tr>
    <th width="40%">event</th>
    <th width="60%">description</th>
  </tr>
  <tr>
    <td>space + click + drag</td>
    <td>Move the Glyph View inside the scroll window.</td>
  </tr>
  <tr>
    <td>⌘ + space + click</td>
    <td>Zoom in.</td>
  </tr>
  <tr>
    <td>⌘ + space + drag</td>
    <td>Zoom to marque rectangle.</td>
  </tr>
  <tr>
    <td>⌥ + ⌘ + space + click</td>
    <td>Zoom out.</td>
  </tr>
  <tr>
    <td>⌥ + mouse scroll</td>
    <td>Zoom in or out.</td>
  </tr>
  <tr>
    <td>drag an image (<code>.png</code>, <code>.jpeg</code>, <code>.tiff</code>) into the Glyph View</td>
    <td>Create an Image object in the current glyph (only one image per layer allowed).</td>
  </tr>
</table>

---
layout: page
title: Scripting Window
tags:
  - scripting
level: beginner
---

* Table of Contents
{:toc}

A simple code editor for writing and running Python scripts.

{% image workspace/scripting-window.png %}

The editor supports syntax highlighting for Python. The colors can be configured in the {% internallink "preferences-window/python" %}.

## Options

Some display options are accessible from the bottom bar:

<table>
  <tr>
    <th width="30%">title</th>
    <th width="70%">description</th>
  </tr>
  <tr>
    <td>Show line numbers</td>
    <td>Show line number before each line.</td>
  </tr>
  <tr>
    <td>Indent using spaces</td>
    <td>Indent code using spaces instead of tabs.</td>
  </tr>
  <tr>
    <td>Window float on top</td>
    <td>Make the Scripting Window float on top of other windows.</td>
  </tr>
  <tr>
    <td>Toggle path browser</td>
    <td>Show/hide the Script Browser.</td>
  </tr>
</table>

## Toolbar

The toolbar gives access to the Scripting Window’s main functions:

<table>
  <tr>
    <th width="30%">title</th>
    <th width="70%">description</th>
  </tr>
  <tr>
    <td>Run</td>
    <td>Execute the current script.</td>
  </tr>
  <tr>
    <td>Comment</td>
    <td>Comment the selected line(s).</td>
  </tr>
  <tr>
    <td>Uncomment</td>
    <td>Uncomment the selected line(s).</td>
  </tr>
  <tr>
    <td>Indent</td>
    <td>Add indentation to selected line(s).</td>
  </tr>
  <tr>
    <td>Dedent</td>
    <td>Remove indentation from selected line(s).</td>
  </tr>
  <tr>
    <td>Save</td>
    <td>Save the current script as a <code>.py</code> file.</td>
  </tr>
  <tr>
    <td>Reload</td>
    <td>Reload the script from disk. (Useful if the script has been edited by another application.)</td>
  </tr>
  <tr>
    <td>New</td>
    <td>Create a new empty script.</td>
  </tr>
  <tr>
    <td>Open</td>
    <td>Open an existing script from <code>.py</code> file.</td>
  </tr>
  <tr>
    <td>Edit With…</td>
    <td>Edit script with another application.</td>
  </tr>
</table>

## Script Browser

The Script Browser is a collapsible side panel which makes it possible to navigate through all scripts in a chosen folder.

{% image workspace/scripting-window_script-browser.png %}

Double-clicking a script in the Script Browser will open it in the code editor.

To change the scripts folder, click on the PopUp button and choose another one.

To show/hide the Script Browser, click on the panel icon in the bottom bar.

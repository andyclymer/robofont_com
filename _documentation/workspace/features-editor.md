---
layout: page
title: Features Editor
tags:
  - OpenType
  - features
level: beginner
---

* Table of Contents
{:toc}

The Features Editor is a simple code editor for writing Open Type features using the [Adobe feature syntax].

{% image workspace/features-editor.png %}

> If you kern using {% internallink 'kern-center' %} or [MetricsMachine], the kerning data is stored in the font’s kerning dictionary, and will be converted automatically into `kern` feature when the font is generated.
>
> If a `kern` feature is defined in the feature code, it will override the font’s kerning dictionary.
{: .note }

> For an introduction to OpenType features, see Tal Leming’s [OpenType Cookbook].
{: .seealso }

[Adobe feature syntax]: http://www.adobe.com/devnet/opentype/afdko/topic_feature_file_syntax.html
[OpenType Cookbook]: http://opentypecookbook.com/
[MetricsMachine]: http://tools.typesupply.com/metricsmachine.html

## Linking to external feature files

The Features Editor supports linking to external `.fea` files using the `include` statement:

{% image workspace/features-editor_include.png %}

```text
include(features.fea);
```

The editor also supports drag-and-drop of `.fea` files. The dropped file is inserted as a link to the external `.fea` path.

## Editing stand-alone feature files

RoboFont supports `.fea` as a native file format. Double-clicking a `.fea` file will open it in a separate Feature Editor which is not connected to any UFO.

{% image workspace/features-editor_stand-alone.png %}

<table>
  <tr>
    <th width="35%">title</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>Insert In Fonts</td>
    <td>Open a sheet to add the features to the selected fonts.</td>
  </tr>
  <tr>
    <td>Save</td>
    <td>Save the feature file.</td>
  </tr>
  <tr>
    <td>Reload</td>
    <td>Reload the feature file from disk.</td>
  </tr>
</table>

## Features menu

More options related to Features are available from the main application menu, under File > Features.

{% image workspace/features-editor_export.png %}

<table>
  <tr>
    <th width="35%">title</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>New Feature</td>
    <td>Create a new feature document, disconnected from any UFO file. It will be saved as a <code>.fea</code> file.</td>
  </tr>
  <tr>
    <td>Export Feature</td>
    <td>Export the features from the current font to a <code>.fea</code> file.</td>
  </tr>
</table>

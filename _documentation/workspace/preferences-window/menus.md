---
layout: page
title: Menu Preferences
treeTitle: Menus
level: beginner
---

The Menus section allows you to define custom keyboard shortcuts for every menu item in RoboFont.

{% image workspace/preferences_menus.png %}

## Adding keyboard shortcut for menu item

1. Find in the list the menu item you wish to edit.
2. Double-click on the Short Key column in this item.
3. A text input field will pop up next to the table cell.
4. Press the shortcut keys on your keyboard – they will appear in the floating input field. To clear a shortcut, use the cross icon at the right.
5. Click anywhere in the table to close the popup and save the shortcut.

{% image workspace/preferences_menus_edit.png %}

> If the same shortcut is used for two menu items, it will work only with the first one.
{: .note }

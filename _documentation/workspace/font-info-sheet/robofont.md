---
layout: page
title: RoboFont
tags:
level: beginner
---

The RoboFont settings contains info specific to RoboFont, for example preferences for generating fonts.

{% image workspace/font-info_robofont.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Decompose</td>
    <td>If checked, the font’s components will be decomposed when a font is generated.</td>
  </tr>
  <tr>
    <td>Remove Overlap</td>
    <td>If checked, overlap will be removed when the font is generated.</td>
  </tr>
  <tr>
    <td>Auto Hint</td>
    <td>If checked, the font will be auto-hinted with the Adobe FDK autohinter (PostScript fonts) or ttfautohint (TrueType fonts, only if it’s installed in the system) using data from the PostScript hinting info.</td>
  </tr>
  <tr>
    <td>Release Mode</td>
    <td>If checked, will set the Adobe FDK in release mode when generating the font. This will turn on subroutinization (makes final OTF file smaller), applies your desired glyph order, and removes the word <em>Development</em> from the font’s version string.</td>
  </tr>
  <tr>
    <td>Format</td>
    <td>Which format to generate the font as: OTF, TTF, or PFA.</td>
  </tr>
  <tr>
    <td>Italic Slant Offset</td>
    <td>Will shift your glyphs horizontally to the left when generating binaries. The number must have a negative value. This is used mainly while drawing italics with slanted metrics.</td>
  </tr>
  <tr>
    <td>Spline Conversion</td>
    <td>When changing between Cubic (PostScript) and Quadratic (TrueType) curves, the conversion can either preserve the number of points or preserve the curves (adding points when necessary).</td>
  </tr>
  <tr>
    <td>Add Dummy DSIG table</td>
    <td>Add a dummy digital signature table while generating a binary font file.</td>
  </tr>
</table>

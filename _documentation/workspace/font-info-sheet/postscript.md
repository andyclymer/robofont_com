---
layout: page
title: PostScript
tags:
  - PostScript
  - hinting
level: beginner
---

* Table of Contents
{:toc}

The PostScript settings contain information related to {% glossary PostScript fonts %}, used by various tables and functions of the {% glossary AFDKO %}.

## Identification

Font name values for PostScript.

{% image workspace/font-info_postscript_identification.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>FontName</td>
    <td>Name to be used for the <code>FontName</code> field in the <code>CFF</code> table.</td>
  </tr>
  <tr>
    <td>FullName</td>
    <td>Name to be used for the <code>FullName</code> field in the <code>CFF</code> table.</td>
  </tr>
  <tr>
    <td>WeightName</td>
    <td>A text description of the weight of the font. It should correspond to the OS/2 <code>usWeightClass</code> value.</td>
  </tr>
  <tr>
    <td>Unique ID Number</td>
    <td>The Postscript Unique ID number.</td>
  </tr>
</table>

### WeightName values

<table>
  <tr>
    <th width='25%'>flag</th>
    <th width='75%'>description</th>
  </tr>
  <tr>
    <td>100</td>
    <td>Thin</td>
  </tr>
  <tr>
    <td>200</td>
    <td>Extra-light</td>
  </tr>
  <tr>
    <td>300</td>
    <td>Light</td>
  </tr>
  <tr>
    <td>400</td>
    <td>Normal</td>
  </tr>
  <tr>
    <td>500</td>
    <td>Medium</td>
  </tr>
  <tr>
    <td>600</td>
    <td>Semi-bold</td>
  </tr>
  <tr>
    <td>700</td>
    <td>Bold</td>
  </tr>
  <tr>
    <td>800</td>
    <td>Extra-bold</td>
  </tr>
  <tr>
    <td>900</td>
    <td>Black</td>
  </tr>
</table>

## Hinting

Values to control the output of the PostScript autohinter.

{% image workspace/font-info_postscript_hinting.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>BlueValues</td>
    <td>A list of up to 14 space separated numbers (integers or floats) specifying the values that should be in the <code>CFF</code> table <code>BlueValues</code> field.¹ <b>These values are a font’s primary alignment zones.</b> <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>OtherBlues</td>
    <td>A list of up to 10 integers or floats specifying the values that should be in the <code>CFF</code> table <code>OtherBlues</code> field.¹ <b>These values are a font’s secondary alignment zones.</b> <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>FamilyBlues</td>
    <td>A list of up to 14 space separated numbers (integers or floats) specifying the values that should be in the <code>CFF</code> table <code>FamilyBlues</code> field.¹ This sets <code>BlueValues</code> across a typeface family, and should be the same for each font in the family. <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>FamilyOtherBlues</td>
    <td>A list of up to 10 space separated numbers (integers or floats) specifying the values that should be in the <code>CFF</code> table <code>FamilyOtherBlues</code> field.¹ This sets <code>OtherBlues</code> values across a typeface family, and should be the same for each font in the family. <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>StemSnapH</td>
    <td>List of horizontal stems sorted in increasing order. Up to 12 space separated numbers (integers or floats) are possible. This corresponds to the <code>CFF</code> table <code>StemSnapH</code> field. <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>StemSnapV</td>
    <td>List of vertical stems sorted in increasing order. Up to 12 space-separated numbers (integers or floats) are possible. This corresponds to the <code>CFF</code> table <code>StemSnapV</code> field. <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>BlueFuzz</td>
    <td>The number of units to extend the effect of an alignment zone to compensate for slightly inaccurate point data.</td>
  </tr>
  <tr>
    <td>BlueShift</td>
    <td>Adjusts the point at which character features outside of an alignment zone will overshoot.</td>
  </tr>
  <tr>
    <td>BlueScale</td>
    <td>BlueScale value. This corresponds to the <code>CFF</code> table <code>BlueScale</code> field. This controls the point size at which overshoot suppression is turned off.²</td>
  </tr>
  <tr>
    <td>ForceBold</td>
    <td>Turning this on this will instruct the font interpreter that it can thicken stems that would render at 1px on screen.</td>
  </tr>
</table>

¹ This list must contain an even number of integers following the rules defined in the Type 1 / CFF specification.

² The formula to determine BlueScale at 300dpi is `(pointsize - 0.49) / 240`.

## Dimensions

{% image workspace/font-info_postscript_dimensions.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>SlantAngle</td>
    <td>Artificial slant angle.</td>
  </tr>
  <tr>
    <td>UnderlineThickness</td>
    <td>Underline thickness value. Corresponds to the <code>post</code> table <code>UnderlineThickness</code> field.</td>
  </tr>
  <tr>
    <td>UnderlinePosition</td>
    <td>Underline position value. Corresponds to the <code>post</code> table <code>UnderlinePosition</code> field.</td>
  </tr>
  <tr>
    <td>isFixedPitched</td>
    <td>Indicates if the font is monospaced. A compiler could calculate this automatically, but the designer may wish to override this setting. This corresponds to the <code>CFF</code> table <code>isFixedPitched</code> field.</td>
  </tr>
  <tr>
    <td>DefaultWidthX</td>
    <td>Default width for glyphs.</td>
  </tr>
  <tr>
    <td>NominalWidthX</td>
    <td>Nominal width for glyphs.</td>
  </tr>
</table>

## Characters

{% image workspace/font-info_postscript_characters.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Default Character</td>
    <td>The name of the glyph that should be used as the default character in PFM files. This is not used in OpenType.</td>
  </tr>
  <tr>
    <td>Microsoft Character Set</td>
    <td>The Windows character set. This is not used in OpenType.</td>
  </tr>
</table>

> - [post table (UFO3 specification)](http://unifiedfontobject.org/versions/ufo3/fontinfo.plist#postscript-specific-data)
> - [post table (OpenType specification)](http://www.microsoft.com/typography/otspec/post.htm)
> - [The Compact Font Format Specification (CFF)](http://www.adobe.com/content/dam/acom/en/devnet/font/pdfs/5176.CFF.pdf)
{: .seealso }

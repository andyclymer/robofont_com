---
layout: page
title: Miscellaneous
tags:
level: beginner
---

The Miscellaneous settings include other kinds of data.

## FOND Data

This section contains information specific to the FOND table.

{% image workspace/font-info_misc_fond.png %}

> This info is not used in OpenType fonts.
{: .note }

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Font Name</td>
    <td>Font name for the FOND resource.</td>
  </tr>
  <tr>
    <td>Family ID Number</td>
    <td>Family ID number. Corresponds to the <code>ffFamID</code> in the FOND resource.</td>
  </tr>
</table>

> - [Macintosh FOND Resource Data (UFO3 specification)](http://unifiedfontobject.org/versions/ufo3/fontinfo.plist#macintosh-fond-resource-data)
> - [Adobe FOND Specification](http://www.adobe.com/content/dam/Adobe/en/devnet/font/pdfs/0091.Mac_Fond.pdf).
{: .seealso }

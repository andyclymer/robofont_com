---
layout: page
title: OpenType
tags:
level: beginner
---

* Table of Contents
{:toc}

The OpenType settings contain information specific to OpenType fonts.

## gasp table

The `gasp` table contains information about the preferred rasterization techniques for when the font is rendered.

{% image workspace/font-info_opentype_gasp.png %}

The `gasp` table consists of a header followed by groupings of gasp records.

Use the +/- buttons to add new records to the `gasp` table.

### gasp record

Each gasp record consists of an integer and a set of flags:

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>rangeMaxPPEM</td>
    <td>Upper limit of range, in PPEM.</td>
  </tr>
  <tr>
    <td>Gridfit</td>
    <td>Use gridfitting.</td>
  </tr>
  <tr>
    <td>Grayscale</td>
    <td>Use grayscale rendering.</td>
  </tr>
  <tr>
    <td>Symmetric Gridfit</td>
    <td>Use gridfitting with ClearType symmetric smoothing. Only supported in version 1 gasp.</td>
  </tr>
  <tr>
    <td>Symmetric Smoothing</td>
    <td>Use smoothing along multiple axes with ClearType. Only supported in version 1 gasp.</td>
  </tr>
</table>

<!--
At very small sizes:
: the best appearance on grayscale devices can usually be achieved by rendering the glyphs in grayscale without using hints.

At intermediate sizes:
: hinting and monochrome rendering will usually produce the best appearance

At large sizes:
: the combination of hinting and grayscale rendering will typically produce the best appearance.
-->

> - [gasp table (UFO3 specification)](http://unifiedfontobject.org/versions/ufo3/fontinfo.plist#opentype-gasp-table-fields)
> - [gasp table (OpenType specification)](http://www.microsoft.com/typography/otspec/gasp.htm)
{: .seealso }

## head table

This `head` table gives global information about the font.

{% image workspace/font-info_opentype_head.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>created</td>
    <td>The creation date of the font.</td>
  </tr>
  <tr>
    <td>lowestRecPPEM</td>
    <td>Smallest readable size in pixels. Corresponds to the OpenType <code>head</code> table <code>lowestRecPPEM</code> field.</td>
  </tr>
  <tr>
    <td>flags</td>
    <td>A list of flags which can be set based on the OpenType specification. Most of these flags are related to TrueType hinting.</td>
  </tr>
</table>

### Flags

<table>
  <tr>
    <th width='20%'>flag</th>
    <th width='80%'>description</th>
  </tr>
  <tr>
    <td>0</td>
    <td>Baseline for font at <code>y=0</code>.</td>
  </tr>
  <tr>
    <td>1</td>
    <td>Left side-bearing point at <code>x=0</code>.</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Instructions may depend on point size.</td>
  </tr>
  <tr>
    <td>3</td>
    <td>Force PPEM to integer values for all internal scaler math.</td>
  </tr>
  <tr>
    <td>4</td>
    <td>Instructions may alter advance width.</td>
  </tr>
  <tr>
    <td>11</td>
    <td>Font data is “lossless”.</td>
  </tr>
  <tr>
    <td>12</td>
    <td>Font converted (produce compatible metrics).</td>
  </tr>
  <tr>
    <td>13</td>
    <td>Font optimized for ClearType.</td>
  </tr>
</table>

> - [head table (UFO3 specification)](http://unifiedfontobject.org/versions/ufo3/fontinfo.plist#opentype-head-table-fields)
> - [head table (OpenType specification)](https://www.microsoft.com/typography/otspec/head.htm)
{: .seealso }

## name table

The `name` table contains strings with font naming information, copyright notices, version numbers, etc.

> The information contained in the `name` table can be localized using [name records](#name-records).
{: .note }

{% image workspace/font-info_opentype_name.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Preferred Family Name</td>
    <td>Preferred family name. Corresponds to the OpenType <code>name</code> table name ID 16.</td>
  </tr>
  <tr>
    <td>Preferred Subfamily Name</td>
    <td>Preferred subfamily name. Corresponds to the OpenType <code>name</code> table name ID 17.</td>
  </tr>
  <tr>
    <td>Compatible Full Name</td>
    <td>Compatible full name. Corresponds to the OpenType <code>name</code> table name ID 18.</td>
  </tr>
  <tr>
    <td>WWS Family Name</td>
    <td>WWS family name. Corresponds to the OpenType <code>name</code> table name ID 21.</td>
  </tr>
  <tr>
    <td>WWS Subfamily Name</td>
    <td>WWS Subfamily name. Corresponds to the OpenType <code>name</code> table name ID 22.</td>
  </tr>
  <tr>
    <td>Version</td>
    <td>Version string. Corresponds to the OpenType <code>name</code> table name ID 5.</td>
  </tr>
  <tr>
    <td>Unique ID</td>
    <td>Unique ID string. Corresponds to the OpenType <code>name</code> table name ID 3.</td>
  </tr>
  <tr>
    <td>Description</td>
    <td>Description of the font. Corresponds to the OpenType <code>name</code> table name ID 10.</td>
  </tr>
  <tr>
    <td>Sample Text</td>
    <td>Sample text. Corresponds to the OpenType <code>name</code> table name ID 20. <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>Name records</td>
    <td>A list of name records (see <a href="#name-records">below</a>). This storage area is intended for records that require platform, encoding and/or language localization.</td>
  </tr>
</table>

> - [name table (UFO3 specification)](http://unifiedfontobject.org/versions/ufo3/fontinfo.plist#opentype-name-table-fields)
> - [name table (OpenType specification)](http://www.microsoft.com/typography/otspec/name.htm)
{: .seealso }

### name records

The `name` table may include translations for multiple languages using name records.

Each name record is made of the following attributes:

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>NID</td>
    <td>Name ID</td>
  </tr>
  <tr>
    <td>PID</td>
    <td>Platform ID</td>
  </tr>
  <tr>
    <td>EID</td>
    <td>Encoding ID</td>
  </tr>
  <tr>
    <td>LID</td>
    <td>Language ID</td>
  </tr>
  <tr>
    <td>string</td>
    <td>A string of characters.</td>
  </tr>
</table>

> - {% internallink "how-tos/adding-localized-name-table" %}
{: .seealso }

## hhea table

The `hhea` table contains information for horizontal layout.

{% image workspace/font-info_opentype_hhea.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Ascender</td>
    <td>Ascender value. Corresponds to the OpenType <code>hhea</code> table <code>Ascender</code> field.</td>
  </tr>
  <tr>
    <td>Descender</td>
    <td>Descender value. Corresponds to the OpenType <code>hhea</code> table <code>Descender</code> field.</td>
  </tr>
  <tr>
    <td>LineGap</td>
    <td>Line gap value. Corresponds to the OpenType <code>hhea</code> table <code>LineGap</code> field.</td>
  </tr>
  <tr>
    <td>caretSlopeRise</td>
    <td>Used to set the slope of the text cursor (rise/run). Use <code>1</code> for vertical. Corresponds to the OpenType <code>hhea</code> table <code>caretSlopeRise</code> field.</td>
  </tr>
  <tr>
    <td>caretSlopeRun</td>
    <td>Used in conjunction with caretSlopeRise. Use <code>0</code> for vertical. Corresponds to the OpenType <code>hhea</code> table <code>caretSlopeRun</code> field.</td>
  </tr>
  <tr>
    <td>caretOffset</td>
    <td>The amount to shift a slanted highlight to produce the best appearance. Set to <code>0</code> for non-slanted fonts. Corresponds to the OpenType <code>hhea</code> table <code>caretOffset</code> field.</td>
  </tr>
</table>

> - [hhea table (UFO3 specification)](http://unifiedfontobject.org/versions/ufo3/fontinfo.plist#opentype-hhea-table-fields)
> - [hhea table (OpenType specification)](http://www.microsoft.com/typography/otspec/hhea.htm)
{: .seealso }

## vhea table

The `vhea` table contains information for vertical layout.

{% image workspace/font-info_opentype_vhea.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>vertTypoAscender</td>
    <td>Vertical ascender value. Distance from the centerline to the previous line’s descent. Corresponds to the OpenType <code>vhea</code> table <code>vertTypoAscender</code> field.</td>
  </tr>
  <tr>
    <td>vertTypoDescender</td>
    <td>Vertical descender value. Distance from the centerline to the next line’s ascent. Corresponds to the OpenType <code>vhea</code> table <code>vertTypoDescender</code> field.</td>
  </tr>
  <tr>
    <td>vertTypoLineGap</td>
    <td>Line gap value. Corresponds to the OpenType <code>vhea</code> table <code>vertTypoLineGap</code> field.</td>
  </tr>
  <tr>
    <td>caretSlopeRise</td>
    <td>Vertical caret slope rise value. A value of <code>0</code> for the rise and a value of <code>1</code> for the run specifies a horizontal caret. A value of <code>1</code> for the rise and a value of <code>0</code> for the run specifies a vertical caret. Corresponds to the OpenType <code>vhea</code> table <code>caretSlopeRise</code> field.</td>
  </tr>
  <tr>
    <td>caretSlopeRun</td>
    <td>Vertical caret slope run value. See the <code>caretSlopeRise</code> field. Corresponds to the OpenType <code>vhea</code> table <code>caretSlopeRun</code> field.</td>
  </tr>
  <tr>
    <td>caretOffset</td>
    <td>Vertical caret offset value. The amount to shift a slanted highlight to produce the best appearance. Set to <code>0</code> for non-slanted fonts. Corresponds to the OpenType <code>vhea</code> table <code>caretOffset</code> field.</td>
  </tr>
</table>

> - [vhea table (UFO3 specification)](http://unifiedfontobject.org/versions/ufo3/fontinfo.plist#opentype-vhea-table-fields)
> - [vhea table (OpenType specification)](http://www.microsoft.com/typography/otspec/vhea.htm)
{: .seealso }

## OS/2 table

The `OS/2` table contains a set of metrics and other data that are required in OpenType fonts.

{% image workspace/font-info_opentype_os2.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>usWidthClass</td>
    <td>Width class value. Corresponds to the OpenType <code>OS/2</code> table <code>usWidthClass</code> field. <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>usWeightClass</td>
    <td>Weight class value. Must be a positive integer. Corresponds to the OpenType <code>OS/2</code> table <code>usWeightClass</code> field. <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>fsSelection</td>
    <td>Options:</td>
  </tr>
  <tr>
    <td>achVendID</td>
    <td>Four character identifier for the creator of the font. Corresponds to the OpenType <code>OS/2</code> table <code>achVendID</code> field. <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>fsType</td>
    <td>The allowed type of embedding for the font. <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>ulUnicodeRange</td>
    <td>A list of supported Unicode ranges in the font. Corresponds to the OpenType <code>OS/2</code> table <code>ulUnicodeRange1</code>, <code>ulUnicodeRange2</code>, <code>ulUnicodeRange3</code> and <code>ulUnicodeRange4</code> fields. <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>ulCodePageRange</td>
    <td>A list of supported code page ranges in the font. Corresponds to the OpenType <code>OS/2</code> table <code>ulCodePageRange1</code> and <code>ulCodePageRange2</code> fields. <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>sTypoAscender</td>
    <td>Ascender value. Corresponds to the OpenType <code>OS/2</code> table <code>sTypoAscender</code> field.</td>
  </tr>
  <tr>
    <td>sTypoDescender</td>
    <td>Descender value. Must be <code>0</code> or a negative number. Corresponds to the OpenType <code>OS/2</code> table <code>sTypoDescender</code> field.</td>
  </tr>
  <tr>
    <td>sTypoLineGap</td>
    <td>Line gap value. Corresponds to the OpenType <code>OS/2</code> table <code>sTypoLineGap</code> field. <span class="not-required">Recommended</span></td>
  </tr>
  <tr>
    <td>usWinAscent</td>
    <td>Ascender value. Corresponds to the OpenType <code>OS/2</code> table <code>usWinAscent</code> field.</td>
  </tr>
  <tr>
    <td>usWinDescent</td>
    <td>Descender value. Must be <code>0</code> or a positive number. Corresponds to the OpenType <code>OS/2</code> table <code>usWinDescent</code> field.</td>
  </tr>
  <tr>
    <td>ySubscriptXSize</td>
    <td>Subscript horizontal font size. Corresponds to the OpenType <code>OS/2</code> table <code>ySubscriptXSize</code> field.</td>
  </tr>
  <tr>
    <td>ySubscriptYSize</td>
    <td>Subscript vertical font size. Corresponds to the OpenType <code>OS/2</code> table <code>ySubscriptYSize</code> field.</td>
  </tr>
  <tr>
    <td>ySubscriptXOffset</td>
    <td>Subscript x offset. Corresponds to the OpenType <code>OS/2</code> table <code>ySubscriptXOffset</code> field.</td>
  </tr>
  <tr>
    <td>ySubscriptYOffset</td>
    <td>Subscript y offset. Corresponds to the OpenType <code>OS/2</code> table <code>ySubscriptYOffset</code> field.</td>
  </tr>
  <tr>
    <td>ySuperscriptXSize</td>
    <td>Superscript horizontal font size. Corresponds to the OpenType <code>OS/2</code> table <code>ySuperscriptXSize</code> field.</td>
  </tr>
  <tr>
    <td>ySuperscriptYSize</td>
    <td>Superscript vertical font size. Corresponds to the OpenType <code>OS/2</code> table <code>ySuperscriptYSize</code> field.</td>
  </tr>
  <tr>
    <td>ySuperscriptXOffset</td>
    <td>Superscript x offset. Corresponds to the OpenType <code>OS/2</code> table <code>ySuperscriptXOffset</code> field.</td>
  </tr>
  <tr>
    <td>ySuperscriptYOffset</td>
    <td>Superscript y offset. Corresponds to the OpenType <code>OS/2</code> table <code>ySuperscriptYOffset</code> field.</td>
  </tr>
  <tr>
    <td>yStrikeoutSize</td>
    <td>Strikeout size. Corresponds to the OpenType <code>OS/2</code> table <code>yStrikeoutSize</code> field.</td>
  </tr>
  <tr>
    <td>yStrikeoutPosition</td>
    <td>Strikeout position. Corresponds to the OpenType <code>OS/2</code> table <code>yStrikeoutPosition</code> field.</td>
  </tr>
  <tr>
    <td>Panose</td>
    <td>The Panose classification for the font.</td>
  </tr>
</table>

### usWeightClass values

<table>
  <tr>
    <th width='20%'>value</th>
    <th width='80%'>description</th>
  </tr>
  <tr>
    <td>100</td>
    <td>Thin</td>
  </tr>
  <tr>
    <td>200</td>
    <td>Extra-light (Ultra-light)</td>
  </tr>
  <tr>
    <td>300</td>
    <td>Light</td>
  </tr>
  <tr>
    <td>400</td>
    <td>Normal (Regular)</td>
  </tr>
  <tr>
    <td>500</td>
    <td>Medium</td>
  </tr>
  <tr>
    <td>600</td>
    <td>Semi-bold (Demi-bold)</td>
  </tr>
  <tr>
    <td>700</td>
    <td>Bold</td>
  </tr>
  <tr>
    <td>800</td>
    <td>Extra-bold (Ultra-bold)</td>
  </tr>
  <tr>
    <td>900</td>
    <td>Black (Heavy)</td>
  </tr>
</table>

### usWidthClass values

<table>
  <tr>
    <th width='20%'>value</th>
    <th width='80%'>description</th>
  </tr>
  <tr>
    <td>1</td>
    <td>Ultra-condensed</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Extra-condensed</td>
  </tr>
  <tr>
    <td>3</td>
    <td>Condensed</td>
  </tr>
  <tr>
    <td>4</td>
    <td>Semi-condensed</td>
  </tr>
  <tr>
    <td>5</td>
    <td>Medium (normal)</td>
  </tr>
  <tr>
    <td>6</td>
    <td>Semi-expanded</td>
  </tr>
  <tr>
    <td>7</td>
    <td>Expanded</td>
  </tr>
  <tr>
    <td>8</td>
    <td>Extra-expanded</td>
  </tr>
  <tr>
    <td>9</td>
    <td>Ultra-expanded</td>
  </tr>
</table>

### fsSelection values

<table>
  <tr>
    <th width='10%'>bit</th>
    <th width='30%'>definition</th>
    <th width='60%'>description</th>
  </tr>
  <tr>
    <td>1</td>
    <td>UNDERSCORE</td>
    <td>Characters are underscored.</td>
  </tr>
  <tr>
    <td>2</td>
    <td>NEGATIVE</td>
    <td>Characters have their foreground and background reversed.</td>
  </tr>
  <tr>
    <td>3</td>
    <td>OUTLINED</td>
    <td>Outlined characters.</td>
  </tr>
  <tr>
    <td>4</td>
    <td>STRIKEOUT</td>
    <td>Characters are over-struck.</td>
  </tr>
  <tr>
    <td>7</td>
    <td>USE_TYPO_METRICS</td>
    <td>Use OS/2 Typo values for ascender, descender, and line gap.</td>
  </tr>
  <tr>
    <td>8</td>
    <td>WWS</td>
    <td>Font has name table strings consistent with a weight/width/slope family without requiring the WWS name values.</td>
  </tr>
  <tr>
    <td>9</td>
    <td>OBLIQUE</td>
    <td>Font is oblique.</td>
  </tr>
</table>

### fsType values

<table>
  <tr>
    <th width='20%'>bit</th>
    <th width='80%'>description</th>
  </tr>
  <tr>
    <td>0</td>
    <td>No embedding restrictions</td>
  </tr>
  <tr>
    <td>1</td>
    <td>No embedding</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Only preview and print embedding allowed</td>
  </tr>
  <tr>
    <td>3</td>
    <td>Editable embedding allowed. Additionally, one can choose to allow subsetting and/or only bitmap embedding.</td>
  </tr>
</table>

> - [OS/2 table (UFO3 specification)](http://unifiedfontobject.org/versions/ufo3/fontinfo.plist#opentype-os2-table-fields)
> - [OS/2 table (OpenType specification)](http://www.microsoft.com/typography/otspec/os2.htm)
{: .seealso }

---
layout: page
title: Extension Builder
tags:
  - extensions
level: beginner
---

A dedicated window for building and editing RoboFont Extensions.

{% image workspace/extension-builder.png %}

## Properties

<table>
  <tr>
    <th width="35%">title</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>Plugin Name</td>
    <td>Name of the extension</td>
  </tr>
  <tr>
    <td>Plugin Icon</td>
    <td>The file can have a custom icon, if set.</td>
  </tr>
  <tr>
    <td>Version</td>
    <td>Version of the extension.</td>
  </tr>
  <tr>
    <td>Developer</td>
    <td>Developer of the extension.</td>
  </tr>
  <tr>
    <td>Developer’s URL</td>
    <td>Developer URL of the extension.</td>
  </tr>
  <tr>
    <td>HTML help root</td>
    <td>Root folder where the HTML help can be found. An <code>index.html</code> file is required.</td>
  </tr>
  <tr>
    <td>Resources root</td>
    <td>Root folder where all the resources can be found. Resources are all necessary files except Python files.</td>
  </tr>
  <tr>
    <td>Script Root</td>
    <td>
      <p>Root folder where all the necessary Python files can be found.</p>
      <dl>
        <dt>Launch during start up</dt>
        <dd>A bool indicating that a script should be executed during RoboFont start up. Use the list to select the script.</dd>
        <dt>Add Script to main menu</dt>
        <dd>Select which scripts should be added to the Extensions menu item. Additionally, a nice name and shortcut can be set for each Python file in the script root.</dd>
      </dl>
    </td>
  </tr>
  <tr>
    <td>Requires RoboFont</td>
    <td>The minimum required version of RoboFont.</td>
  </tr>
</table>

## Actions

<table>
  <tr>
    <th width="35%">title</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>Build</td>
    <td>Build the extension in a given folder.</td>
  </tr>
  <tr>
    <td>Import</td>
    <td>Import data from an existing extension.</td>
  </tr>
  <tr>
    <td>Clear</td>
    <td>Empty all fields in the Extension Builder.</td>
  </tr>
  <tr>
    <td>Close</td>
    <td>Close the Extension Builder.</td>
  </tr>
</table>

> - {% internallink "extensions/building-extensions-with-extension-builder" %}
{: .seealso }

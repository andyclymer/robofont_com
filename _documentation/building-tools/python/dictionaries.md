---
layout: page
title: Dictionaries
level: beginner
---

* Table of Contents
{:toc}

So far we’ve looked at several data types in Python: *strings*, *integers*, *floats*, *lists*, *tuples*, *booleans*. Now let’s have a look at dictionaries.

A dictionary is an *unordered* collection of *key/value pairs*. *Unordered collection* means that in a dictionary, different than in lists or tuples, the items have no order.

The items of a dictionary are accessed by their *keys*, not by their order. Every value in a dictionary has a key, and vice-versa. The key is usually a string, the value can be anything (including another dictionary).

## Dictionary basics

Dictionaries are written with curly brackets. Here’s how we create an empty dict:

    myDict = {}

And here's a new dictionary with some values:

    myDict = {
        'key 1' : 'value',
        'key 2' : 'another value',
    }

We can add new items to a dictionary with the following syntax:

    myDict['key 3'] = 'some other value'

Individual items can be accessed by their keys:

``` python
print(myDict['key 2'])
```

``` console
>>> another value
```

If there is no item with the given key, a `KeyError` is raised:

``` python
print(myDict['key 4'])
```

``` console
>>> Traceback (most recent call last):
>>>   File "<untitled>", line 2, in <module>
>>> KeyError: 'key 4'
```

## Dictionary methods

Every dictionary has a few methods which we can use to access and manipule their data.

We can get all keys or all values of a dictionary as lists:

``` python
print(dict.keys())
```

``` console
>>> ['key 1', 'key 2', 'key 3']
```

``` python
print(myDict.values())
```

``` console
>>> ['value', 'another value', 'some other value']
```

We can also get all items in a dictionary is a list of key/value tuples:

``` python
print(myDict.items())
```

``` console
>>> [('key 1', 'value'), ('key 2', 'another value'), ('key 3', 'some other value')]
```

The `has_key` method allows us to ask if a dictionary has an item with this key:

``` python
print(myDict.has_key('key 1'))
```

``` console
>>> True
```

``` python
print(myDict.has_key('key 4'))
```

``` console
>>> False
```

Here’s another way to write the same thing, by asking if the list of keys has a certain key:

``` python
print('key 1' in myDict.keys())
```

``` console
>>> True
```

``` python
print('key 4' in myDict.keys())
```

``` console
>>> False
```

## Avoiding KeyErrors

We can use conditionals to avoid getting a `KeyError` for non-existing keys:

    if 'key 4' in myDict.keys():
        print(myDict['key 4'])
    else:
        print('key not in dict')

Dictionaries also have a `get` method, which returns `None` if the key does not exist:

``` python
print(myDict.get('key 1'))
```

``` console
>>> another value
```

``` python
print(myDict.get('key 4'))
```

``` console
>>> None
```

## Iterating over a dictionary

Depending on what is more useful to the problem at hand, we can iterate through a dictionary’s keys retrieving their values:

``` python
for key in myDict.keys():
    print(key, myDict[key])
```

``` console
>>> key 1 value
>>> key 2 another value
>>> key 3 some other value
```

...or we can iterate through key/value pairs directly:

``` python
for key, value in myDict.items():
    print(key, value)
```

``` console
>>> key 1 value
>>> key 2 another value
>>> key 3 some other value
```

> - include explanation and examples of `OrderedDict`
> - include sorting dict by values?
{: .todo }

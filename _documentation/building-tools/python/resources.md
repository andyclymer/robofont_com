---
layout: page
title: Additional resources
level: beginner
---

There are many resources for learning Python on the web. Here are a few to get you started:

- [How To Think Like a Computer Scientist](http://openbookproject.net/thinkcs/python/english2e/index.html)
- [Python Practice Book](http://anandology.com/python-practice-book/index.html)
- [Learn Python The Hard Way](http://learnpythonthehardway.org/book/)

## Tutorials


- [How To Think Like a Computer Scientist](http://openbookproject.net/thinkcs/python/english2e/index.html)
- [Dive Into Python](http://www.diveintopython.net/)
- [Python Practice Book](http://anandology.com/python-practice-book/index.html)
- [80+ Best Free Python Tutorials](http://www.fromdev.com/2014/03/python-tutorials-resources.html)
- [Tutorial de Python (em português)](http://turing.com.br/pydoc/2.7/tutorial/index.html)

## History


- [Python programming language](https://en.wikipedia.org/wiki/Python_(programming_language)) – Wikipedia
- [History of Python](https://en.wikipedia.org/wiki/History_of_Python) – Wikipedia
- [The History of Python](http://python-history.blogspot.nl/) – Guido van Rossum
- [Python’s Design Philosophy](http://python-history.blogspot.nl/2009/01/pythons-design-philosophy.html) – Guido van Rossum
- [A Brief Timeline of Python](http://python-history.blogspot.nl/2009/01/brief-timeline-of-python.html) – Guido van Rossum

---
layout: page
title: External modules
level: beginner
---

* Table of Contents
{:toc}

## Some useful modules

[grapefruit](https://github.com/xav/Grapefruit>)
: working with colors in different color modes, creating gradients etc.

[markdown](https://pythonhosted.org/Markdown/)
: generating html from Markdown sources

*some other module*
: *what the module can be used for*

## Installing modules

### Installing with pip

...

### Installing with setup scripts

...

### Installing with git clone

...

### Installing manually with a .pth file

Create a simple text file in a code editor, containing the path to the root folder where the module lives.

> The easiest way to get the correct path is by dragging the folder from Finder into a code editor or Terminal – so you’ll get the path without having to type it.
{: .tip }

Save this file with the name of the module and the extension `.pth` in the `site-packages` folder for the desired Python(s). For example:

``` console
/Library/Python/2.7/site-packages/myModule.pth
```

And that’s it.

This method creates a *reference* to the folder in which the module lives.

## Testing a module

To see if a module is installed, just try to import it in the environment you wish to work in:

    import myModule

If no error is raised, you’re good to go.

> - include other useful external packages
> - expand installation instructions
> - inlude markdown, grapefruit, etc. examples?
{: .todo }

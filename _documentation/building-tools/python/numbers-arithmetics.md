---
layout: page
title: Numbers and arithmetics
level: beginner
---

* Table of Contents
{:toc}

Now let’s have a look at some objects in Python which are used to represent numbers.

## Integers and decimal numbers

The two main ones are: `int` (integers) and `float` (decimal numbers):

``` python
# this is an integer
A = 12
print(type(A))
```

``` console
>>> <type 'int'>
```

``` python
# this is a float
B = 12.5
print(type(B))
```

``` console
>>> <type 'float'>
```

The only difference in terms of notation if the use of a period for decimal numbers. A number is a decimal number even if there is no digit after the period:

``` python
# this is still a float
C = 12.
print(type(C))
```

``` console
>>> <type 'float'>
```

## Arithmetic Operations

### Addition

Adding numbers is easy and intuitive: simply use the `+` (plus) operator :

``` python
print(1 + 1)
```

``` console
>>> 2
```

``` python
print(1 + 1 + 10)
```

``` console
>>> 12
```

Integers can be added to integers, floats can be added to floats, and integers can be added to floats (and vice-versa):

``` python
# result is an integer
print(12 + 13)
```

``` console
>>> 25
```

``` python
# result is a float
print(12 + 0.5)
```

``` console
>>> 12.5
```

``` python
# result is a float
print(0.5 + 12.5)
```

``` console
>>> 13.0
```

If both numbers are integers, the result is also an integer. If at least one of the numbers is a float, the result will be a float.

### Subtraction

Subtracting numbers follows the same logic, just use the `-` (minus) operator:

``` python
print(12 - 8)
```

``` console
>>> 4
```

The minus sign is also used to indicate negative numbers:

``` python
print(12 - 25)
```

``` console
>>> -13
```

### Multiplication

In Python, the multiplication operator is the `*` character, rather than `×` (multiplication sign) as usual in maths. But it works exactly the same way:

``` python
print(12 * 8)
```

``` console
>>> 96
```

``` python
print(12 * -25)
```

``` console
>>> -300
```

### Division

Division in Python uses the `/` (forward slash) as its operator:

``` python
print(11 / 2)
```

``` console
>>> 5.5
```

Division by zero is mathematically not possible and will always raise an error:

``` python
print(1 / 0)
```

``` console
>>> Traceback (most recent call last):
>>>   File "<untitled>", line 1, in <module>
>>> ZeroDivisionError: integer division or modulo by zero
```

Integer division can be performed with the special operator `//`:

``` python
print(11 // 2)
```

``` console
>>> 5
```

Finally, use the `%` operator to get the rest of a division (modulo):

``` python
print(11 % 2)
```

``` console
>>> 1
```

### Exponentiation

To elevate a number to the power of another number, the operator `**` is used:

``` python
print(2 ** 8)
print(10 ** 2)
print(2 ** 0.5)
```

### Combining operations

Arithmetic operations can be combined into larger statements and calculations:

    7 + 10 - 100 * 3 / 200 ** 4

Python executes division and multiplication first, and addition and subtraction afterwards. If you want to add or subtract first, you must put these operations between parentheses – Python will execute operations between parentheses first:

``` python
print(9 * 9 + 2)
```

``` console
>>> 83
```

``` python
print(9 * (9 + 2))
```

``` console
>>> 99
```

### Increment/decrement operators

Sometimes when writing code it is necessary to increment or decrement a value (to add or subtract a value from an integer):

    a = 10
    a = a + 2

In cases like this, it possible to write the same line using the increment `+=` operator:

    # a = a + 2
    a += 2

...and a decrement operator `-=` is also available:

    # b = b - 1
    b -= 1

There is also an incrementing product operator `*=`:

    # c = c * 10
    c *= 10

## More math

Python can do many other kinds of mathematical calculations, of course. Many of these mathematical functions live in [the math module]. For example, trigonometric functions such as *sine*, *cosine*, *tangent*; constants such as *pi*, etc.

``` python
import math
print(math.pi)
```

``` console
>>> 3.14159265359
```

[the math module]: https://docs.python.org/2/library/math.html

> - [Numeric Types — int, float, long, complex](https://docs.python.org/2/library/stdtypes.html#typesnumeric)
{: .seealso }

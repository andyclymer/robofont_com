---
layout: page
title: Sets
level: beginner
---

* Table of Contents
{:toc}

Sets are a special kind of unorderered collection which contains only unique elements, and has special methods to perform set operations like *union*, *intersection*, *difference* etc.

## Set basics

Sets can be created from a list or tuple using the `set` constructor. Notice how all duplicated items are removed, and appear only once int the resulting set:

``` python
set1 = set([1, 3, 2, 3, 4, 3])
print(set1)
```

``` console
>>> set([1, 2, 3, 4])
```

### New syntax for creating sets

Since Python 2.7, non-empty sets can be created by enclosing a sequence of comma-separated values in curly brackets:

``` python
set2 = {1, 8, 4, 5, 6}
print(set2)
```

``` console
>>> set([8, 1, 4, 5, 6])
```

Curly brackets are also used to create dictionaries. So a pair of empty curly brackets will create a dictionary, not a set:

``` python
print(type({}))
```

``` console
>>> <type 'dict'>
```

To create an empty set, it is still required to use the `set` constructor:

``` python
print(type(set()))
```

``` console
>>> <type 'set'>
```

## Adding and removing set items

``` python
set3 = {37, 51, 42, 60}
set3.add(98)
print(set3)
```

# set([98, 42, 51, 60, 37])
set3.remove(51)
print(set3)
set([98, 42, 60, 37])



``` python
print(type(set()))
```

s.add(x)        add element x to set s
s.remove(x)

## Set operations

Sets have methods to perform boolean operations with other sets: *union*, *difference* and *intersection*.

The `union` operation returns a new set with elements from both sets:

``` python
print(set_1)
print(set_2)
```

``` console
>>> set([1, 2, 3, 4])
>>> set([8, 1, 4, 5, 6])
```

``` python
print(set_1.union(set_2))
```

``` console
>>> set([1, 2, 3, 4, 5, 6, 8])
```

The `intersection` operation returns a new set with elements which are common to both sets:

``` python
print(set_1.intersection(set_2))
```

``` console
>>> set([1, 4])
```

The `difference` operation returns a new set with elements which are in the first set, and not in the second:

``` python
print(set_1.difference(set_2))
```

``` console
>>> set([2, 3])
```

``` python
print(set_2.difference(set_1))
```

``` console
>>> set([8, 5, 6])
```

> For the `difference` operation, the order of the sets matters. For the other operations, changing the order will not change the result.
{: .note }

---
layout: page
title: Comparisons
level: beginner
---

* Table of Contents
{:toc}

When we have two or more variables, it is often necessary to write code that makes comparisons between them.

When comparing two objects it is important to make a distinction between value and identity:

- they can have different value and different identities
- they can have the same value, but have separate identities
- they can have the same value *and* the same identity

Let’s have a look at how to ask these questions in Python:

## Comparing values

Let’s start by storing a few numbers in variables:

``` python
a = 12
b = 20
c = 20
```

We can now compare these values in different ways. The result of a comparison expression is always a boolean value.

Are the numbers equal? This question is asked with the `==` operator:

``` python
print(a == b)
```

``` console
>>> False
```

``` python
print(b == c)
```

``` console
>>> True
```

Are the numbers different (not equal)? This question is asked with the `!=` operator:

``` python
print(a != b)
```

``` console
>>> True
```

``` python
print(b != c)
```

``` console
>>> False
```

Is the first number bigger than the second? This question is asked with the `>` operator:

``` python
print(a > b)
```

``` console
>>> False
```

``` python
print(b > c)
```

``` console
>>> False
```

Is the first number smaller than the second? This question is asked with the `<` operator:

``` python
print(a < b)
```

``` console
>>> True
```

``` python
print(b < c)
```

``` console
>>> False
```

Is the first number bigger than or equal to the second? This question is asked with the `>=` operator:

``` python
print(a >= b)
```

``` console
>>> False
```

``` python
print(b >= c)
```

``` console
>>> True
```

Is the first number smaller than or equal to the second? This question is asked with the `<=` operator:

``` python
print(a <= b)
```

``` console
>>> True
```

``` python
print(b <= c)
```

``` console
>>> True
```

## Comparing identity

Besides comparing the *value* of two objects, we can also compare their identity.

Two objects can have the same value and still have different *identities* – they are not the same thing. For example:

The integer `10` and the decimal number `10.0` have the same nummerical value:

``` python
print(10 == 10.0)
```

``` console
>>> True
```

To compare the identity between the objects we use the logical operator `is`:

``` python
print(10 is 10.0)
```

``` console
>>> False
```

As we can see, the two objects have different identities – one is a `float`, and the other is an `int`.

## Testing ‘truthiness’

Every value in every data type in Python can be converted to a boolean. Empty objects are usually converted to `False`, while anything else is converted to `True`.

Here are some examples with different data types:

``` python
# string
print(bool('hello'))
print(bool(''))
```

``` console
>>> True
>>> False
```

``` python
# list
print(bool(['a', 'b', 'c']))
print(bool([]))
```

``` console
>>> True
>>> False
```

``` python
# tuple
print(bool((1, 2, 3)))
print(bool(()))
```

``` console
>>> True
>>> False
```

``` python
# dict
print(bool({'A' : 1, 'B' : 2}))
print(bool({}))
```

``` console
>>> True
>>> False
```

``` python
# integer
print(bool(100))
print(bool(0))
```

``` console
>>> True
>>> False
```

``` python
# float
print(bool(1.1))
print(bool(0.0))
```

``` console
>>> True
>>> False
```

``` python
# None
print(bool(None))
```

``` console
>>> False
```

> - {% internallink "python/collections-loops#testing-item-membership" text="Collections and Loops: testing item membership" %}
> - [comparisons](https://docs.python.org/2/library/stdtypes.html#comparisons)
{: .seealso }

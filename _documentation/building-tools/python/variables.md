---
layout: page
title: Variables
level: beginner
---

* Table of Contents
{:toc}

Variables are simply names with values assigned to them. A few examples:

``` python
a = 12
b = 15
t = "a string"
```

Variables can be used in expressions. When the code is executed, they get substituted by the value they stand for:

``` python
a * b
```

``` console
>>> 180
```

In Python, everything is an object. The following assignment ‘restarts’ the variable named `a`, connecting it to another object:

``` python
a = a + 10
print(a)
```

``` console
>>> 22
```

## Naming variables

Variable names can’t start with a number. If you try it, a `SyntaxError` will be raised:

``` python
1a = 12
```

``` console
>>> Traceback (most recent call last):
>>>   File "<untitled>", line 1
>>>     1a = 12
>>>      ^
>>> SyntaxError: invalid syntax
```

Variable names can contain numbers, as long as they are not the first character:

    a1 = 12

Underscores are also allowed in variable names — and have a special meaning too, indicating private names (more about this later):

    _a = 12
    a_ = 13

Variable names are case sensitive. So `x` is a different variable than `X`:

``` python
x = 12
X = 13
print(x, X, x == X)
```

``` console
>>> 12 13 False
```

Therefore, this will raise a `NameError`:

``` python
y = 102
print(Y)
```

``` console
>>> Traceback (most recent call last):
>>>   File "<untitled>", line 2, in <module>
>>> NameError: name 'Y' is not defined
```

## Assigning multiple items

When declaring variables, several items can be assigned at once:

    x, y, z = 0, 100, 200

This only works if the amount of variables and the amount of values are the same:

``` python
print(x, y, z = 0, 100)
```

``` console
>>> Traceback (most recent call last):
>>>   File "<untitled>", line 1, in <module>
>>> ValueError: need more than 2 values to unpack
```

If two or more variables have the same value, they can be assigned at once:

``` python
x = y = z = 100
print(x, y, z)
```

``` console
>>> 100 100 100
```

## Swapping values

In Python, we can swap the values of two variables at once with the following syntax:

``` python
a, b = 10, 20
a, b = b, a
print(a, b)
```

``` console
>>> 20 10
```

---
layout: page
title: Introduction
level: beginner
---

* Table of Contents
{:toc}

Thousands of people from different fields have learned Python, and so can you. It’s just a language, and a way of thinking in terms of rules, systems and patterns. **There is no magic involved!** With a bit of time and effort you too can become fluent in Python.

## Some quick facts about Python

- The Python programming language was developed by Guido van Rossum in the late 1980s at the Centrum Wiskunde & Informatica in The Netherlands.

- Python is a *high-level* programming language with emphasis on readability. ‘High level’ means that it is distant from the 0s and 1s from the computer, and closer to human language (English, in this case).

- Python is free/open-source software and is available for all major platforms.

- Because Python is a *dynamic* programming language (executed at runtime), it is also often used as a scripting language in various kinds of applications.

## Why is Python called Python?

The name ‘Python’ was inspired by the the British comediants group [Monty Python](https://en.wikipedia.org/wiki/Monty_python).

{% image python/monty3.jpeg caption='Programming does not have to be serious and boring!' %}

## Python and typeface design

Python plays an important role in type design and font production. Most font editors today support scripting with Python.

> Expand this section with a bit more background info about Python and type design (RoboFog, RoboFab etc).
>
> - [Python in Typeface and Font Development: An Interview with Just van Rossum](https://talkpython.fm/episodes/show/47/python-in-typeface-and-font-development)
> - [FontParts – History](https://fontparts.readthedocs.io/en/latest/index.html#history)
{: .todo }

## Python 2 vs. Python 3

Python 3 is a new version of Python which is not fully compatible with the 2.X branch. Some of the new features in Python 3 have been ported backwards to Python 2.6 and 2.7, and are available via the `__future__` module.

Python 3 is still not the default version in many production environments, since several important libraries have not been updated. Many systems still come with some version of Python 2.X pre-installed (for example, as of writing, the latest version of macOS comes with Python 2.7.2).

## Python’s Design Philosophy

According to Guido van Rossum, the following guidelines helped to make decisions while designing and implementing Python:

- Borrow ideas from elsewhere whenever it makes sense.
- “Things should be as simple as possible, but no simpler.” (Einstein)
- Do one thing well (The “UNIX philosophy”).
- Don’t fret too much about performance – plan to optimize later when needed.
- Don’t fight the environment and go with the flow.
- Don’t try for perfection because “good enough” is often just that.
- (Hence) it’s okay to cut corners sometimes, especially if you can do it right later.

### The Zen of Python

The philosophy behind the design of the Python language is summarized in a series of aphorisms called *The Zen of Python*. This text is available as an ‘easter egg’ in all Python distributions. Simply type the following line of code in your favorite Python console:

    import this

...and the complete version of *The Zen of Python* will be printed as output:

``` console
>>> Beautiful is better than ugly.
>>> Explicit is better than implicit.
>>> Simple is better than complex.
>>> Complex is better than complicated.
>>> Flat is better than nested.
>>> Sparse is better than dense.
>>> Readability counts.
>>> ...
```

Try it out for yourself to read the rest.

<!--
> ```
> import antigravity
> ```
> ```
> from __future__ import braces
> ```
{: .seealso }
-->

---
layout: page
title: Environments
level: beginner
---

* Table of Contents
{:toc}

## Where can I run Python code?

Python can be used in many different environments. Some applications already come with an embedded Python interpreter, so you can use Python as a scripting language to automate things that you would otherwise have to make ‘by hand’ (clicking around in menus and icons).

Besides RoboFont, most font editors today also support scripting with Python: [FontLab], [Glyphs], [FontForge].

[FontLab]: http://fontlab.com/python-scripting/
[Glyphs]: https://docu.glyphsapp.com/
[FontForge]: http://www.fontforge.org/python.html

## Writing Python code in RoboFont

RoboFont comes with a handy {% internallink "workspace/scripting-window" %}, a simple code editor where you can write and run your Python code.

{% image python/scripting-window.png %}

## DrawBot

DrawBot is an enviroment to create 2D graphics with Python code. It was originally developed by Just van Rossum to teach programming to design students at the [KABK].

{% image python/drawbot.png %}

To learn more about DrawBot, have a look at the [DrawBot documentation]. For the brave, DrawBot’s source-code is [available on Github].

DrawBot is also available as a [RoboFont extension]. This version is specially useful for typeface designers: because it lives inside the font editor, it has direct access to the full RoboFont API, so you can use the current font objects directly in your scripts.

[KABK]: http://kabk.nl/
[DrawBot documentation]: http://drawbot.com/
[available on Github]: https://github.com/typemytype/drawbot
[RoboFont extension]: https://github.com/typemytype/drawBotRoboFontExtension

## Python in Terminal

Python can be used directly in a console or command-line interface, outside of an existing application.

On Mac OS X, this can be done via the Terminal, which can be found in your *Applications / Utilities* folder.

After launching Terminal, you will see something like this:

``` console
Last login: Wed Jul 23 05:01:17 on ttys000
username:~ username$
```

This is your default command-line prompt, with your own username.

There are two ways to use Python on the console: interactive mode, or running an existing script.

### Interactive mode

To enter interactive mode in Terminal, simply type `python` in the command-line:

``` console
username:~ username$ python
```

You will probably see something like this:

``` console
>>> Python 2.7.5 (default, Aug 25 2013, 00:04:04)
>>> [GCC 4.2.1 Compatible Apple LLVM 5.0 (clang-500.0.68)] on darwin
>>> Type "help", "copyright", "credits" or "license" for more information.
>>>
```

This command gives us some information about the current Python installation, and returns an interactive Python prompt.

We can type Python code in this prompt, and it gets executed as it goes.

Let's start with a simple *hello world*:

``` console
"hello world"
```

``` console
>>> 'hello world'
```

The same goes for mathematical expressions, in fact for any Python expression:

``` console
1 + 1
```

``` console
>>> 2
```

> In interactive mode, it is not necessary to use the `print` statement – expressions are executed and return the result.
{: .note }

It is also possible to write multi-line code in the interactive mode. Notice how the prompt changes to indicate that it is waiting for input:

``` console
for i in range(7):
...     print(i,)
...
```

``` console
>>> 0 1 2 3 4 5 6
```

### Running existing scripts

The interactive mode is useful for writing quick tests and short scripts, but it is not really suitable for working with larger pieces of code. When that is the case, it is better to write scripts as separate `.py` files, and use Terminal only to run them.

Let's suppose we have the following Python script, which prints "hello world" as output:

``` python
print('hello world')
```

This script is stored as a separate `hello.py` file.

To call this file from Terminal, we simply call the command `python`, followed by the path to the script file. A tip: instead of typing out the full path, you can simply drag the file from Finder to the Terminal:

``` console
username:~ username$ python /Users/username/Desktop/hello.py
```

The output will be, as expected:

``` console
>>> hello world
```

## Python in SublimeText

It is also possible to run Python scripts from inside SublimeText (our favorite code editor). To run a script, use the key combination `Cmd+B` — this will execute your code and print the output to the console at the bottom of the editor.

{% image python/sublime-text.png %}

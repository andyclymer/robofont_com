---
layout: page
title: Building extensions
tree:
  - extension-file-spec
  - from-tool-to-extension
  - boilerplate-extension
  - building-extensions-with-extension-builder
  - building-extensions-with-script
  - recommendations-publishing
  - publishing-extensions
  - packaging-patterns
  - publishing-commercial-extensions
treeCanHide: true
level: intermediate
---

RoboFont is a robust core application which is fully scriptable, so it can be customized and extended using Python.

In addition to its open and documented APIs, RoboFont also offers an Extensions infra-structure which makes it easy for developers to build and distribute extensions, and for users to install and update them.

**Extensions are an easy way to package and distribute RoboFont tools.**

{% image extensions/extension-icon.png %}

In this section you’ll find everything you need to know about building and distributing RoboFont extensions.

<!--
to-do: add option to choose between long and short titles in tree
{% tree page.url levels=1 %}
-->

- {% internallink "extension-file-spec" %}
- {% internallink "from-tool-to-extension" %}
- {% internallink "boilerplate-extension" %}
- {% internallink "building-extensions-with-extension-builder" %}
- {% internallink "building-extensions-with-script" %}
- {% internallink "recommendations-publishing" %}
- {% internallink "publishing-extensions" %}
- {% internallink "packaging-patterns" %}
- {% internallink "publishing-commercial-extensions" %}

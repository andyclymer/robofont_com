---
layout: page
title: Interactive tools
level: advanced
---

Some tools can be developed to react to glyph window events like mouse position and clicks, keystrokes, modified keys etc.

## Examples

- Simple tool example (1)
- Simple tool example (2)
- Polygonal selection tool
- Preview multiple fonts

---
layout: page
title: Glyph Preview
tags:
  - mojo
level: intermediate
---

The GlyphPreview object is a simple glyph preview area for use with vanilla windows.

The example script below also shows `OpenWindow`, a small helper function which prevents RoboFont from opening the same window twice.

{% image building-tools/mojo/GlyphPreview.png %}

{% showcode building-tools/mojo/glyphPreviewExample.py %}

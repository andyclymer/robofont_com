---
layout: page
title: Accordion View
tags:
  - mojo
level: intermediate
---

RoboFont’s {% internallink "workspace/inspector" text="Inspector panel" %} uses a custom UI component, the Accordion View. It consists of multiple sections which can be expanded/collapsed as needed – this can be useful when there’s more information than screen space to display it.

{% image building-tools/mojo/AccordionView.png %}

{% showcode building-tools/mojo/accordionViewExample.py %}

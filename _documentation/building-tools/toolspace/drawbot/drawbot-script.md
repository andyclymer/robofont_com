---
layout: page
title: Using DrawBot in scripts
tags:
    - scripting
    - drawBot
level: intermediate
---

DrawBot can also be used as a regular module, without the UI. You can import it directly into your scripts, and save the output to disk. This also works outside of RoboFont.

The example below will save a pdf file in your Desktop folder.

{% showcode building-tools/drawbot/useDrawBotAsModule.py %}

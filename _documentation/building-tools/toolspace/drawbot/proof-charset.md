---
layout: page
title: Proof character set
tags:
    - scripting
    - drawBot
level: intermediate
---

A simple character set proofer using the DrawBot extension in RoboFont.

All glyphs are displayed side-by-side following the order defined in the `f.glyphOrder` attribute. You can adjust some basic parameters such as the page margin, and size/spacing of the glyph boxes.

{% image building-tools/drawbot/proof-charset.png %}

{% showcode building-tools/drawbot/proofCharset.py %}

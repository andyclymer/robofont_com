---
layout: page
title: Proof glyphs
tags:
    - scripting
    - drawBot
level: intermediate
---

This example generates glyph proofs for the specified glyphs, showing only one glyph per page – together with font metrics, glyph box, anchors, and glyph info such as glyph name, unicode, width, and left/right side-bearings.

Glyphs can be specified as a glyph object, integer (glyph index) or list slice (glyph order).

Each glyph is placed at the center of the page. The glyph scale and page size can be defined by the user.

{% image building-tools/drawbot/proof-glyphs.png %}

{% showcode building-tools/drawbot/proofGlyphs.py %}

---
layout: page
title: Bubbles preview
tags:
    - observers
level: advanced
---

This example shows how to draw something in the canvas every time you preview a glyph.

{% showcode building-tools/custom-tools/bubblesPreview.py %}

---
layout: page
title: The Observer Pattern
tags:
    - scripting
    - observers
level: advanced
---

Explain what the Observer Pattern is and how it works.

...

> [Tal Leming: Building Apps (RoboThon 2012)] + [partial transcript]
{: .seealso }

[Tal Leming: Building Apps (RoboThon 2012)]: http://vimeo.com/116064787#t=4m30s
[partial transcript]: http://gist.github.com/gferreira/18b9af056a9d4319d207d5a6c7ee7b2c

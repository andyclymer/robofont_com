---
layout: page
title: Event Observer
tags:
    - observers
level: advanced
---

This is a useful tool when learning to work with observers in RoboFont: it shows when each observer is fired, and the contents of each notification.

{% image building-tools/eventObserver.png %}

Don’t worry too much about understanding the code in this example. Focus on learning the different notifications. Open a font, click around in the font window, glyph window and space center – look at the updates in the notifications list.

{% showcode building-tools/custom-tools/eventObserver.py %}

---
layout: page
title: How to get a font
tags:
    - scripting
    - FontParts
level: beginner
---

* Table of Contents
{:toc}

The first step when writing a script to do something to a font is, well, getting the font.

There are different ways to get a font:

## CurrentFont

For example, you can choose to do something to a font which is already open in RoboFont:

``` python
f = CurrentFont()
print(f)
```

``` console
>>> <Font MyFont Regular>
```

If several fonts are open, `CurrentFont` will return the one which is currently at the top. If no font is open, `CurrentFont` will return `None`.

## OpenFont

When there is no font open, you can choose to open an existing font:

    f = OpenFont()

This will open a dialog, so you can locate the font in your machine. When you find the font and click on the *Open* button, a new font window will be opened for this font.

If you already know where the font file is located, you can pass its path as an argument to `OpenFont`:

    f = OpenFont(u"/Users/me/Desktop/myfont.ufo")

> To get get the full path to a file without having to type it all out, simply drag the file into the Scripting Window.
{: .tip }

You can also choose to ‘open’ a font without actually opening a font window for it. This can be useful when working with very large fonts, which can make your scripts slower.

Here’s how you would do this:

``` python
f = OpenFont(u"/Users/me/Desktop/myfont.ufo", showInterface=False)
print(f)
```

``` console
>>> <Font MyFont Regular>
```

There is your font, in memory. You can now write more code to do things to it. Because the font has no interface, there is no visual feedback to show the result of your actions. Don’t forget to save the font after you’re done.

> In RoboFont 1.8, use `showUI` instead of `showInterface`:
>
> ``` python
> f = OpenFont(u"/Users/me/Desktop/myfont.ufo", showUI=False)
> ```
{: .note }

## NewFont

There will be cases in which you will need to create a new font from scratch, instead of using an existing one. In these cases, use `NewFont`:

``` python
f = NewFont()
print(f)
```

``` console
>>> <Font None None>
```

If you don’t need a font window for it, use the `showInterface=False` argument like we did with `OpenFont` above:

    f = NewFont(showInterface=False)

## AllFonts

In some cases there will be not just one, but several fonts open at once in RoboFont. And we might want to do something to *all* these fonts at tonce. This is a job for `AllFonts`:

``` python
for f in AllFonts():
    print(f)
    # do something
```

``` console
>>> <Font None None>
>>> <Font MyOtherFont Regular>
>>> <Font MyFont Bold>
>>> <Font MyFont Regular>
```

In the above example we have fonts from different families, and with different styles. What if we want to do something only to some of these fonts? Well, `AllFonts` offers some handy functions to help with that:

``` python
for f in AllFonts().getFontsByFamilyName('MyFont'):
    print(f)
    # do something
```

``` console
>>> <Font MyFont Bold>
>>> <Font MyFont Regular>
```

``` python
for f in AllFonts().getFontsByStyleName('Regular'):
    print(f)
    # do something
```

``` console
>>> <Font MyOtherFont Regular>
>>> <Font MyFont Regular>
```

<!--
# sort by any `font.info` attribute

fonts = AllFonts("openTypeOS2WeightClass")
for font in fonts:
    print(font.info.openTypeOS2WeightClass)
-->

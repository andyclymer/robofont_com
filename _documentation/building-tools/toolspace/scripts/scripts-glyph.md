---
layout: page
title: Scripts to do things to glyphs
tags:
    - scripting
    - FontParts
level: intermediate
---

* Table of Contents
{:toc}

## Move points by a random amount of units

This script messes up with your outlines by moving every point by a random amount of units. While not very useful, it shows randomness and Undo in action.

{% showcode building-tools/glyphs/movePointsRandom.py %}

> In RoboFont 1.8, use `glyph.update()` instead of `glyph.changed()`.
{: .note }

## Add anchor to selected glyphs

This script adds top anchors at the specified y-position in the selected glyphs.

The x-position is calculated as the middle of each glyphs’s bounding box.

{% showcode building-tools/glyphs/addAnchors.py %}

> In RoboFont 1.8, use `glyph.box()` instead of `glyph.bounds()`.
{: .note }

## Drawing inside a glyph with a pen

This example shows how to draw into a glyph with code – using a Pen object.

{% showcode building-tools/glyphs/drawWithPen.py %}

## Rasterizing a glyph shape

The script below shows a simple glyph rasterizer using `glyph.pointInside()`.

{% showcode building-tools/glyphs/rasterizeGlyph.py %}

```console
- - - - - - - - - - - - - - - - -
- - - - - - - - - - - - - - - - -
- - - - - - - - - - - - - - - - -
- - - - - - - - - - - - - O O - -
- - - - - - O O O O O - O O O O -
- - - - O O O O O O O O O - - - -
- - - O O O O - - - O O O O - - -
- - - O O O - - - - O O O O - - -
- - O O O O - - - - - O O O - - -
- - O O O O - - - - - O O O - - -
- - - O O O - - - - O O O O - - -
- - - O O O O - - - O O O - - - -
- - - - - O O O O O O O - - - - -
- - - - - - - - O - - - - - - - -
- - - - - O O O - - - - - - - - -
- - - - O O O O O O O O - - - - -
- - - - - - O O O O O O O - - - -
- - - - - O O - - - O O O - - - -
- - - - O O O - - - O O O - - - -
- - - - O O O O - - O O O - - - -
- - - - - O O O O O O - - - - - -
- - - - - - - - - - - - - - - - -
```

---
layout: page
title: Scripting environment
tags:
    - scripting
    - FontParts
level: beginner
---

* Table of Contents
{:toc}

## Scripting Window

RoboFont’s primary scripting environment is the {% internallink "workspace/scripting-window" %}, a code editor which allows you to write, open, edit and run Python scripts.

{% image workspace/scripting-window.png %}

The output of scripts which are run in the Scripting Window appear in the bottom part of the window.

### Font objects out-of-the-box

When writing code in the Scripting Window, the main font objects are available out-of-the-box – so you can use `CurrentFont`, `OpenFont`, `NewFont`, `AllFonts`, `CurrentGlyph` etc. directly, without the need to import them at the top of your scripts.

<!-- {% image building-tools/ScriptingWindow.png %} -->

``` python
print("hello world!")
print(CurrentFont())
print(CurrentGlyph())
```

``` console
>>> hello world!
>>> <RFont 'RoboType Roman' at 4864862160>
>>> <RGlyph 'G' ('foreground') at 4872730960>
```

The FontParts objects are available only to scripts which are run in the Scripting Window. If your main script calls a function from an external module, you’ll need to import the font objects explicitly from `mojo.roboFont` in that file:

``` python
# getFont.py
from mojo.roboFont import CurrentFont

def getFont():
    return CurrentFont()
```

<!-- {% image building-tools/ScriptingWindow-3.png %} -->

``` python
from getFont import getFont

print(getFont())
```

``` console
>>> <RFont 'RoboType Roman' at 4853462928>
```
{: .output }

<!--

## Scripts list

The Scripting Window offers a simple file browser which allows you to navigate through a folder of scripts. You can turn it on/off by clicking on the icon at the bottom left.

Scripts in the current scripts folder will be listed at the bottom of the Extensions menu. In RF3, scripts get their own entry in the main application menu.

RF3 also allows the user to set a custom title for scripts. This title appears in the Scripts menu, instead of the file name, so it can be improved for legibility (it can be longer, it can use spaces etc).

    # menuTitle : my funky script
    # shortCut  : command+shift+alt+t

-->

### Code interaction

Like [DrawBot’s code editor](http://drawbot.com/content/drawBotApp/codeEditor.html), RoboFont’s Scripting Window offers special ways to interact with values in your program. When selected, some kinds of Python objects can be modified interactively by pressing the ⌘ key and using the mouse or arrow keys.

{% include embed vimeo=89407186 %}

The interaction depends on the type of object:

- boolean values behave like an on/off switch
- integers behave like sliders
- pairs of numbers behave like movement in x/y space

<table>
  <tr>
    <th colspan="5">bool</th>
  </tr>
  <tr>
    <td width="30%">⌘</td>
    <td width="70%" colspan="4">Turn value on/off.</td>
  </tr>
  <tr>
    <th colspan="5">integer</th>
  </tr>
  <tr>
    <td width="30%"></td>
    <td colspan="2">↑</td>
    <td colspan="2">↓</td>
  </tr>
  <tr>
    <td><em>⌘</em></td>
    <td colspan="2">+1</td>
    <td colspan="2">-1</td>
  </tr>
  <tr>
    <td>⌘ + ⌥</td>
    <td colspan="2">+0.1</td>
    <td colspan="2">-0.1</td>
  </tr>
  <tr>
    <td>⌘ + ⇧</td>
    <td colspan="2">+10</td>
    <td colspan="2">-10</td>
  </tr>
  <tr>
    <td>⌘ + ⌥ + ⇧</td>
    <td colspan="2">+0.01</td>
    <td colspan="2">-0.01</td>
  </tr>
  <tr>
    <th colspan="5">pairs of numbers</th>
  </tr>
  <tr>
    <td width="30%"></td>
    <td width="17.5%">→</td>
    <td width="17.5%">←</td>
    <td width="17.5%">↑</td>
    <td width="17.5%">↓</td>
  </tr>
  <tr>
    <td><em>⌘</em></td>
    <td>x +1</td>
    <td>x -1</td>
    <td>y +1</td>
    <td>y -1</td>
  </tr>
  <tr>
    <td>⌘ + ⌥</td>
    <td>x +0.1</td>
    <td>x -0.1</td>
    <td>y +0.1</td>
    <td>y -0.1</td>
  </tr>
  <tr>
    <td>⌘ + ⇧</td>
    <td>x +10</td>
    <td>x -10</td>
    <td>y +10</td>
    <td>y -10</td>
  </tr>
  <tr>
    <td>⌘ + ⌥ + ⇧</td>
    <td>x +0.01</td>
    <td>x -0.01</td>
    <td>y +0.01</td>
    <td>y -0.01</td>
  </tr>
</table>

## Output Window

The {% internallink "workspace/output-window" %} is also important while developing your tools or debugging someone else’s tools: this is where print statements and tracebacks appear when a script is not running from the Scripting Window.

{% image building-tools/output-window.png %}


---
layout: page
title: Interpolation scripts
tags:
    - interpolation
    - FontParts
level: intermediate
---

* Table of Contents
{:toc}

> - {% internallink "how-tos/working-with-interpolation" %}
{: .seealso }

## Interpolating colors

A simple DrawBot example showing basic interpolation maths at work.

{% image building-tools/interpolation/interpolate-colors.png %}

{% showcode building-tools/interpolation/interpolateColors.py %}

> Modify color values and number of steps and see how the result changes.
{: .tip}

## Interpolating position and size

Another visual example using DrawBot, this time interpolating position and size instead of RGB color channels.

{% image building-tools/interpolation/interpolate-pos-size.png %}

{% showcode building-tools/interpolation/interpolatePosSize.py %}

## Checking interpolation compatibility

Before interpolating two glyphs, we need to make sure that they are compatible. We can do that in code, using a glyph’s `isCompatible` method. This function returns two values:

- The first value is a boolean indicating if the two glyphs are compatible.
- If the first value is `False`, the second will contain a report of the problems.

```python
f = CurrentFont()
g = f['O']
print(g.isCompatible(f['o']))
print(g.isCompatible(f['n']))
```

```console
>>> (True, '')
>>> (False, '[Fatal] Contour 0 contains a different number of segments.\n[Fatal] Contour 1 contains a different number of segments.\n[Warning] The glyphs do not contain components with exactly the same base glyphs.')
```

> - {% internallink "how-tos/preparing-for-interpolation" %}
{: .seealso }

## Interpolating glyphs in the same font

This script generates a number of interpolation and extrapolation steps between two selected glyphs in the same font.

{% image building-tools/interpolation/interpolate-in-font.png %}

{% showcode building-tools/interpolation/interpolateGlyphs.py %}

## Interpolating glyphs into the current font

In this example, 3 fonts are open: the current font, where the glyphs will be interpolated, and two master fonts, named “Regular” and “Bold”. The master fonts are selected by their style names using `AllFonts().getFontsByStyleName()`.

{% showcode building-tools/interpolation/interpolateGlyphsIntoCurrentFont.py %}

```console
>>> A not in RoboType Bold, skipping…
>>> interpolating B…
>>> interpolating C…
>>> interpolating a…
>>> interpolating b…
>>> interpolating c…
```

## Interpolating fonts

This example will generate a new font by interpolating two existing fonts. We assume that both fonts are compatible.

{% showcode building-tools/interpolation/interpolateFonts.py %}

## Batch interpolating fonts

This script generates a series of instances by interpolating two master fonts.

The names and interpolation values of the individual instances are defined in the code as a list of tuples.

{% showcode building-tools/interpolation/batchInterpolateFonts.py %}

```console
>>> generating instances...
>>>
>>>    generating Light (0.25)...
>>>    saving instances at /myFolder/MyFamily_Light.ufo...
>>>
>>>    generating Regular (0.5)...
>>>    saving instances at /myFolder/MyFamily_Regular.ufo...
>>>
>>>    generating Bold (0.75)...
>>>    saving instances at /myFolder/MyFamily_Bold.ufo...
>>>
>>> ...done.
```

## Condensomatic

A script to generate a Condensed from Regular and Bold. A [Type\]Media](http://typemedia.org/) classic.

{% image building-tools/interpolation/condensomatic.png %}

{% showcode building-tools/interpolation/condensomatic.py %}

---
layout: page
title: Simple move example
tags:
  - scripting
  - vanilla
level: intermediate
---

{% showcode /building-tools/vanilla/simpleMoveExample.py %}

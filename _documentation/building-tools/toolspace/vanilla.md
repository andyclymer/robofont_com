---
layout: page
title: Scripts with user interface
tags:
    - scripting
    - vanilla
tree:
    - vanilla-intro
    - vanilla-example-1
    - vanilla-example-slider
    - simple-font-library-browser
treeCanHide: true
level: intermediate
---

The simplest interface to a program is its source code. All tools start off as {% internallink "toolspace/scripts" text="simple scripts" %}, and some often stay like that. This is common when you are creating small scripts for yourself.

Some scripts, however, require an interface between the code and the user – a few checkboxes for selecting options, an input field to define a value, or a button to execute a certain function. Adding a UI to a script is specially useful when you are building tools for others, or when a tool is used very often.

**RoboFont makes it easy to add a UI to your scripts using vanilla.**

## Examples

{% tree page.url levels=1 %}

> - [macOS Human Interface Guidelines](http://developer.apple.com/library/content/documentation/UserExperience/Conceptual/OSXHIGuidelines/)
{: .seealso }

---
layout: page
title: Introduction to Python
tree:
  - introduction
  - environment
  - language
  - numbers-arithmetics
  - variables
  - strings
  - lists
  - tuples
  - sets
  - collections-loops
  - comparisons
  - conditionals
  - boolean-logic
  - dictionaries
  - builtin-functions
  - functions
  - objects
  - standard-modules
  - external-modules
  - custom-modules
  - resources
treeCanHide: true
level: beginner
---

<!--
This section contains a simplified introduction to the Python programming language. It covers just the basics you’ll need to know before getting started with scripting in RoboFont. For those who want to learn more, {% internallink "python/resources" %} are also provided.

- ...
- principles
- style
- naming
- good-practices
- ...

-->

{% tree page.url levels=1 %}

---
layout: page
title: Building extensions with the Extension Builder
treeTitle: Building with Extension Builder
tags:
  - extensions
level: intermediate
---

The {% internallink "workspace/extension-builder" %} allows you to set all required information for your extension manually, using a dialog.

The fields and options of the dialog correspond to the data format attributes described in the {% internallink "extension-file-spec" %}.

The image below shows all settings for building the {% internallink "boilerplate-extension" %}.

{% image building-tools/extensions/extensionBuilder.png caption='the Extension Builder interface' %}

After you have built your extension for the first time, you can import information from the generated extension package using the *import* button at the bottom of the window.

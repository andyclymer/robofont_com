---
layout: page
title: Extension packaging patterns
treeTitle: Packaging patterns
tags:
  - extensions
  - recommendations
level: intermediate
---

* Table of Contents
{:toc}

## Pattern 1

**Extension source and packaged extension are in separate repositories.**

- packaged extension is generated from source via build script
- packaged extension is registered on Mechanic
- development (updates, pull requests) happens in the extension source

> \# project 1
>
> http://github.com/user/repository1
> ├── extension1 code source
> ├── extension1 docs source
> └── extension1 build script
>
> http://github.com/user/repository1_extension
> ├── extension1.roboFontExt
> ├── extension1 README
> └── extension1 docs html
>
> \# project 2
>
> http://github.com/user/repository2
> ├── extension2 code source
> ├── extension2 docs source
> └── extension2 build script
>
> http://github.com/user/repository2_extension
> ├── extension2.roboFontExt
> ├── extension2 README
> └── extension2 docs html
{: .asCode }

### Examples

- hTools2: [source](https://github.com/gferreira/hTools2) / [extension](https://github.com/gferreira/hTools2_extension)

>   This works but seems redundant (two repositories for one tool).
>
>   The release process is also cumbersome:
>
>   1. edit source code
>   2. commit to source repository
>   3. rebuild extension with script
>   4. commit to extension repository
{: .note }

## Pattern 2

**Extension source and packaged extension are in the same repository.**

- one extension per repository
- check if this works in Mechanic

> \# project 1
>
> http://github.com/user/repository1
> ├── extension1 code source
> ├── extension1 docs source
> ├── extension1 build script
> ├── extension1.roboFontExt
> ├── extension1 README
> └── extension1 docs html
>
> \# project 2
>
> http://github.com/user/repository2
> ├── extension2 code source
> ├── extension2 docs source
> ├── extension2 build script
> ├── extension2.roboFontExt
> ├── extension2 README
> └── extension2 docs html
{: .asCode }

## Pattern 3

**Extension source IS the packaged extension.**

- code is written as package format
- one extension per repository

> \# project 1
>
> http://github.com/user/repository1
> ├── extension1.roboFontExt
> ├── extension1 README
> └── extension1 docs html
>
> \# project 2
>
> http://github.com/user/repository2
> ├── extension2.roboFontExt
> ├── extension2 README
> └── extension2 docs html
{: .asCode }

> Note sure how this works… (ask Frederik)
>
> - do I need to re-install the extension every time I want to modify and run the code?
> - do I need to work inside the RoboFont extensions folder?
{: .note }

### Examples

- (look for examples)

## Pattern 4

**Multiple extensions in a single repository.**

- with or without extension sources (patterns 2 or 3)
- how does Mechanic handle this?

> http://github.com/user/repository
> ├── extensions README
> ├── extension1/
> │   ├── (extension1 code source)
> │   ├── (extension1 docs source)
> │   ├── (extension1 build script)
> │   ├── extension1.roboFontExt
> │   ├── extension1 README
> │   └── extension1 docs html
> └── extension2/
>     ├── (extension2 code source)
>     ├── (extension2 docs source)
>     ├── (extension2 build script)
>     ├── extension2.roboFontExt
>     ├── extension2 README
>     └── extension2 docs html
{: .asCode }

### Examples

- [fbOpenTools](https://github.com/FontBureau/fbOpenTools)

---
layout: page
title: Publishing extensions on Mechanic 2
treeTitle: Publishing on Mechanic 2
tags:
  - extensions
level: intermediate
---

The process of publishing an extension on Mechanic 2 is all done using Git and GitHub.

1. Fork the [mechanic-2-server] repository.
2. Add an `<extensionName>.yml` file to the `_data` folder.
3. Make a pull request, and wait until it is approved by one of the admins.

After the merge, your extension will appear on the [Mechanic 2 website], and will become available to the [Mechanic 2 extension].

### Mechanic extension metadata format

Here is an example [yaml] file used by Mechanic:

```yaml
extensionName: myExtension
repository: http://github.com/robodocs/rf-extension-boilerplate
extensionPath: build/myExtension.roboFontExt
description: A boilerplate extension which serves as starting point for creating your own extensions.
developer: RoboDocs
developerURL: https://github.com/roboDocs
tags: [demo]
```

[mechanic-2-server]: https://github.com/robofont-mechanic/mechanic-2-server/
[Mechanic 2 website]: http://robofontmechanic.com/
[Mechanic 2 extension]: http://github.com/robofont-mechanic/mechanic-2
[yaml]: https://en.wikipedia.org/wiki/YAML

---
layout: page
title: From tool to extension
tags:
  - extensions
level: intermediate
---

If you have created a tool and want to make it available to other RoboFont users, you can turn it into a *RoboFont Extension*.

If you know in advance that you want to build an extension, you can start working with an extension folder structure from the beginning.

Before building the extension, make sure that your files are organized according to the {% internallink "extension-file-spec#extension-folder-structure text='Extension Folder Structure' %} specification, separating code, documentation and resources.

{% image building-tools/extensions/extensionFolderStructure.png caption='extension folder structure' %}

> - Individual folder names are arbitrary. When building the extension, you can specify which folder contains what (code, html or resources).
> - The code folder can contain multiple subfolders.
{: .note }

## Boilerplate Extension

If you are building extensions for the first time, it can be helpful to use the {% internallink "boilerplate-extension" text="Boilerplate Extension" %} as a template when organising your files. This will save you some time and prevent errors when building the extension.

## Building the extension

When it’s time to build your extension, there are two options:

- {% internallink "building-extensions-with-extension-builder" %}
- {% internallink "building-extensions-with-script" %}

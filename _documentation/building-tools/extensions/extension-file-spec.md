---
layout: page
title: Extension File Specification
treeTitle: Extension File Spec
tags:
  - extensions
level: intermediate
---

* Table of Contents
{:toc}

RoboFont extensions are [macOS packages] – folders which act like files. They have a [standard folder structure](#extension-folder-structure) and the file extension `.roboFontExt`.

[macOS packages]: https://en.wikipedia.org/wiki/Package_(macOS)

> You can view the contents of a package by right-clicking it in Finder, and selecting *Show Package Contents*.
{: .tip }

## Extension Folder Structure

*Version: 2.0*

Extension folders must have the following internal structure:

> myExtension.robofontExtension
> │
> ├── [info.plist](#infoplist)
> ├── [lib](#lib)
> │   └── \*.py
> ├── ([html](#html))
> │   ├── index.html
> │   └── (\*.html)
> ├── ([resources](#resources))
> │   └── (any type of file)
> ├── ([license](#license))
> └── ([requirements.txt](#requirementstxt))
>
> files and folders in (parentheses) are not required
{: .asCode }

## info.plist

An [XML property list] file with information about the extension.

A technical specification of the format is [available as DTD].

[XML property list]: http://en.wikipedia.org/wiki/Property_list
[available as DTD]: http://www.apple.com/DTDs/PropertyList-1.0.dtd

<table>
<tr>
  <th width='45%'>key</th>
  <th>value</th>
</tr>
{% for data in site.data.extensionsSpec.extensionBundle %}
<tr>
  <td rowspan='3'>
    <code class='highlighter-rouge'>{{ data.key }}</code>
  </td>
  <td>{{ data.description | markdownify }}</td>
</tr>
<tr>
  <td>{{ data.value }}</td>
</tr>
<tr>
  <td class="{{ data.required | slugify }}">{{ data.required | markdownify  }}</td>
</tr>
{% endfor %}
</table>

The `info.plist` can have custom keys if necessary for the extension.

Custom keys should use **reverse domain names** as keys, to avoid name conflicts with other extensions.

Example: `com.myDomain.myExtension.myCustomKey`

> Storing Mechanic data in the extensions’ `info.plist` is deprecated in Mechanic 2. Data for publishing extensions on Mechanic must now be stored in a `.yaml` file in the [Mechanic 2 Server] repository – see [Publishing extensions on Mechanic 2](#).
{: .seealso }

[Mechanic 2 Server]: http://github.com/robofont-mechanic/mechanic-2-server

## lib

A folder containing all the `.py` files needed by the extension.

If a developer does not want to ship readable source code in his extension, it is possible to include only compiled `.pyc` files.

> These `.pyc` files should be made with Python 2.7 (built-in).
{: .note }

When installing the extension, the `lib` folder is added to `sys.path`, so all the other files and folders in the extension can be imported with Python as modules. Because of this, it is advised to use **reverse domain names without dots** to prefix extension modules, to avoid name conflicts with other extensions.

Example: `comMyDomainMyExtensionMyModule`

## html

*Not required.*

If a `html` folder is declared in the `info.plist`, then it should contain a file named `index.html`. This file should be a plain html file, which will be rendered in RoboFont’s {% internallink "building-tools/toolspace/mojo/help-window" %} with [WebKit].

[WebKit]: http://www.webkit.org/

## resources

The `resources` folder is a place to keep any other additional files needed for your extension. It is commonly used for assets, such as images for use in toolbar icons and cursors; or for additional compiled command-line tools.

## license

*Not required.*

The license file contains the license for the extension. It can be a `.txt` or `.html` file.

## requirements.txt

*Not required.*

The `requirements.txt` file contains extension names which are required by the extension. For example, an extension which uses DrawBot requires that the DrawBot extension is installed.

## Menu Item Description

Each menu item description is a dictionary with the following keys:

<table>
<tr>
  <th width='25%'>key</th>
  <th>value</th>
</tr>
{% for data in site.data.extensionsSpec.menuItemDescription %}
<tr>
  <td rowspan='3'>
    <code class='highlighter-rouge'>{{ data.key }}</code>
  </td>
  <td>{{ data.description | markdownify }}</td>
</tr>
<tr>
  <td>{{ data.value }}</td>
</tr>
<tr>
  <td class="{{ data.required | slugify }}">{{ data.required | markdownify }}</td>
</tr>
{% endfor %}
</table>

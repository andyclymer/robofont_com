---
layout: page
title: Boilerplate RoboFont Extension
treeTitle: Boilerplate Extension
tags:
  - extensions
level: intermediate
---

The [Boilerplate Extension] is a dummy RoboFont extension which doesn’t do anything useful. It was made as an example and template, to help you get started when building your own extensions. It is hosted on GitHub, so it’s easy to fork and make changes.

[Boilerplate Extension]: http://github.com/roboDocs/rf-extension-boilerplate

If you look at the source code of extensions by different developers, you will see that there are many different ways to organize files and publish extensions. The Boilerplate Extension shows a simple scheme which should be easy for beginners to understand and follow:

> rf-extension-boilerplate
> ├── source/
> │   ├── code/
> │   ├── documentation/
> │   └── resources/
> ├── build/myExtension.roboFontExt
> │   ├── html/
> │   ├── lib/
> │   ├── resources/
> │   ├── info.plist
> │   └── license
> ├── README.md
> ├── build.py
> └── license.txt
{: .asCode }

Source folder
: The `source` folder contains all source files, separated into sub-folders according to the {% internallink "extension-file-spec#extension-folder-structure text='Extension Folder Structure' %}:

  - code (.py files)
  - documentation (html/css files)
  - resources (icons, images etc)

  **These are the 'master files' which are edited by the developer.**

Build folder
: The `build` folder contains the generated extension package, which can be either {% internallink "building-extensions-with-script" text="built with a script" %} (included in the repository) or {% internallink "building-extensions-with-extension-builder" text="with the Extension Builder" %}.

  **This package is what gets distributed to users via Mechanic.**

Root folder
: The root folder of the repository contains the `build.py` script, which generates the extension package from the source files. It also includes two meta-files for GitHub:

  - a `README` file with information about the repository
  - a `license` file with the license for the repository

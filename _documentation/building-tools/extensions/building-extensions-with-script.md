---
layout: page
title: Building extensions with a script
treeTitle: Building with script
tags:
  - extensions
level: intermediate
---

If you are more comfortable writing scripts than using UIs, you can also build your extension with code.

The script below is part of the {% internallink "boilerplate-extension" %}, and is written to work with the other files in the repository. If you choose to organize your files differently, you will need to update the script accordingly.

{% showcode building-tools/extensions/buildExtension.py %}

> The script uses the exact same settings as the {% internallink "workspace/extension-builder" text="example using the Extension Builder" %}.
{: .note }

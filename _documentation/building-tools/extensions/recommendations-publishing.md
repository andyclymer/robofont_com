---
layout: page
title: Recommendations for Publishing Extensions
treeTitle: Publishing Recommendations
tags:
  - extensions
  - recommendations
level: intermediate
---

* Table of Contents
{:toc}

Here are some general recommendations for developers who are planning to publish extensions.

## One extension per repository

While it is possible to host several extensions in a single repository, this should be avoided.

> - Why exactly? Add some good reasons to justify this recommendation.
{: .todo }

## Naming

Check [existing extensions](/extensions) to avoid name-clashes or confusion with other extensions.

## Uniqueness

If your extension has the exact same functionality as an existing extension, ask yourself: does it make sense to publish it? Sometimes it’s better to contribute ideas and code to an existing extension than creating a new one which is very similar. That’s the beauty of open-source collaboration!

## Including documentation

Unless your extension is extremely simple and self-explaining, it is a good idea to include some documentation for users.

When writing documentation, consider the following angles:

- how things look like (describing the UI: dialogs, menus, buttons etc.)
- how to do things (describing how to perform tasks and accomplish certain results)

> - include examples of extensions which have good documentation
{: .todo }

## Version numbers

Try to use version numbers in a meaningful way.

For example, in version numbers with three digits X.Y.Z:

  - X is the major version (a complete rewrite)
  - Y is the minor version (new features added)
  - Z is a ‘patch’ (bug fixes)

## Metadata for Mechanic / Extension Store

If you plan to distribute your extension with Mechanic, it is important to set the required information correctly.

Think about how this information will be used in context, so that listing on Mechanic and in the [extensions overview](/extensions) is readable, informative and cohesive.

The same recommendations also apply to commercial extensions for distribution via the Extension Store.

## Choose a license wisely

Before releasing an extension, take some time to consider the appropriate license for your code.

If you are publishing an open-source extension on GitHub, you can use [choosealicense.com](http://choosealicense.com/) to help you choose a license. See the [Licenses comparison table](http://choosealicense.com/appendix/) for a detailed overview of the options available.

For commercial extensions, *...insert some text here about what to do when publishing commercial extensions in the Extension Store. RoboFont does not give legal advice etc etc, if you are serious about this then talk to a lawyer. It might be a good idea to have a look around and see how licenses for commercial software (in particular plugins) are structured.*

> Answering the following questions may help you arrive at the right choice of license for your extension.
>
> Are users allowed to…
>
> - modify your code for their own use?
> - redistribute your code?
> - distribute modified versions of your code?
> - charge money for distributing (modified versions of) your code?
{: .tip }

## Shipping source vs. binary code

When building the extension, you have a choice to ship your extension as source code or only in compiled form. This technical setting defines what users can do with your code in practice.

The large majority of RoboFont extensions available today is published as open-source code on GitHub. If your code is open, users are able to read it, study it, and modify it. Other developers can make contributions to improve your tool, but can also take your ideas and use them in another context – if the license allows.

If you ship only compiled code, users will not be able to read or modify your code. This option might make sense for commercial extensions, to enforce the terms of more restrictive licenses and prevent others from accessing your code.

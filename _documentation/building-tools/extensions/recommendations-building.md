---
layout: page
title: Recommendations for Building Extensions
treeTitle: Building Recommendations
tags:
  - extensions
  - recommendations
level: intermediate
---

## User interface

- main reference: Apple’s [macOS Human Interface Guidelines](http://developer.apple.com/library/content/documentation/UserExperience/Conceptual/OSXHIGuidelines/)

## Code

- use tools from the `mojo` module rather than `lib`

    ```
    # this has been deprecated:
    from lib.tools.defaults import getDefault
    # use this instead:
    from mojo.UI import getDefault
    ```

    also:

        from mojo.pens import decomposePen

- suggest camelCase for common libraries?
- give examples of best practices (vanilla, FontParts, DrawBot, …?)
- [Python Style Guide](http://robodocs.info/pythonIntro/source/content/style.html)

## Building the extension

- using the Extension Builder vs. building extension with a script

## Version control

- source code (development) vs. built extension (distribution)
- see {% internallink "extensions/packaging-patterns" %}

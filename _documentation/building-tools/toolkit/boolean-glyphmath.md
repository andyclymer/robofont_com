---
layout: page
title: Boolean Glyph Math
tags:
  - contours
level: beginner
draft: false
---

RoboFont makes it possible to perform common boolean path operations (difference, union, intersection and exclusive or) with glyph objects directly, using special characters as operators.

<table>
  <tr>
    <th width='50%'>operation</th>
    <th width='50%'>operator</th>
  </tr>
  <tr>
    <td>difference</td>
    <td><code>%</code></td>
  </tr>
  <tr>
    <td>union</td>
    <td><code>|</code></td>
  </tr>
  <tr>
    <td>intersection</td>
    <td><code>&amp;</code></td>
  </tr>
  <tr>
    <td>exclusive or</td>
    <td><code>^</code></td>
  </tr>
</table>

Here is an example with the four boolean path operations in action:

{% image building-tools/glyph-booleans1.png %}

{% showcode building-tools/glyphs/booleanGlyphMath.py %}

> For the difference operation, the order of the operands matters!
{: .warning }

---
layout: page
title: Converting from Glyphs to UFO
tags:
  - UFO
draft: true
draft-hidden: true
---

## Exporting UFO from Glyphs app

[Glyphs](https://glyphsapp.com) app has a builtin function for exporting UFO files.

Go to menu: File/Export/UFO (⌘+E)

There you'll have two options:

- *Convert Glyph Names to Production Names*. With this option checked, Glyphs will rename exported glyphs to production friendly names, as Glyphs *nice-naming* scheme won't work 100% with `makeotf` in some cases.
- Only present in latest Beta builds, the *Decompose smart stuff* option will decompose Smart components and Corner components.


### Notes

- **Glyphs** exports only the current front font. It doesn't export active instances if working with a MM file (check helper scripts for batch export and exporting all instances from a MM source file).
- **Glyphs** will export .ufo 2 (by Feb. 26th).
- It will add some private data to your ufo lib.plist file:
    - com.schriftgestaltung.font.license
    - com.schriftgestaltung.font.licenseURL
    - com.schriftgestaltung.font.panose
    - com.schriftgestaltung.disablesAutomaticAlignment
    - com.schriftgestaltung.glyphOrder
    - com.schriftgestaltung.useNiceNames
    - com.schriftgestaltung.fontMasterID
- The whole font will be exported, including glyphs set to "not export". You'll have to take care of removing them later.
- Mark colors are kept when opening in RF3, lost if opening in previous RF1.8
    

### Helpers

A couple of python scripts for Glyphs that might help you to export ufo from the app:

- Export all open fonts to ufo:

    ```python
    # MenuTitle: batch ufo export
    # -*- coding: utf-8 -*-
    
    __doc__ = """Export all open fonts to ufo"""
    
    
    import os.path
    
    
    Exporter = NSClassFromString('GlyphsFileFormatUFO').alloc().init()
    
    for f in Glyphs.fonts:
        Master = f.fontMasters()[0]
        Exporter.setFontMaster_(Master)
        filePath = f.parent.fileURL().path()
        # TODO: make a folder called 'ufo' and save the ufos there
        fileName, folderName = os.path.basename(filePath), os.path.dirname(filePath)
        ufoFolder = os.path.join(folderName, 'ufo')
        ufoFileName = fileName.replace('.glyphs', '.ufo')
    
        if not os.path.exists(ufoFolder):
            os.mkdir(ufoFolder)
    
        ufoFilePath = os.path.join(ufoFolder, ufoFileName)
        Result = Exporter.writeUfo_toURL_error_(f, NSURL.fileURLWithPath_(ufoFilePath), None)
    
    # open folder where the ufo files were saved
    import os
    os.system("open %s" % ufoFolder)
    ```

- Export all instances to ufo:
    
    ```python
    # MenuTitle: instances ufo export
    # -*- coding: utf-8 -*-
    
    
    """
        Export all instances from Glyphs app current font as ufo.
        It saves the ufo file in the same folder than the source files
    """
    
    # import os
    import subprocess
    
    
    ufoExporter = Glyphs.objectWithClassName_("GlyphsFileFormatUFO")
    font = Glyphs.font
    fontFilePath = font.filepath
    
    for instance in Glyphs.font.instances:
        if not instance.active:
            continue
        instanceFont = instance.interpolatedFont
        ufoExporter.setFontMaster_(instanceFont.masters[0])
        folder = os.path.dirname(font.filepath)
        ufoFilePath = os.path.join(folder, font.familyName + "-" + instance.name + ".ufo")
        dest = NSURL.fileURLWithPath_(ufoFilePath)
        ufoExporter.writeUfo_toURL_error_(instanceFont, dest, None)
    
    subprocess.Popen(['open', folder])
    ```
    
### Issues?

(The currently tested **Glyphs** version is **v2.5b(1113)**)

When opening back a .ufo file in Glyphs 2.4.4 (1075), the contour start points indexes are shifted to the previous one. This behaviour doesn't happen with newer v2.5b(1113).

- [fixed in build 1113] ~~An UFO3 saved from RoboFont opens in Glyphs, if you save in Glyphs doesn't open in RoboFont as *layercontents.plist* is missing:~~

---

Further reading: [Working with UFO](https://glyphsapp.com/tutorials/working-with-ufo) tutorial in Glyphs app website.

---

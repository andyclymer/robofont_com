---
layout: page
title: Introduction to OpenType variations
tags:
  - variable fonts
  - interpolation
  - OpenType
level: intermediate
draft: true
draft-hidden: true
---

* Table of Contents
{:toc}

<!--
*A brief introduction to variable fonts for beginners.*
-->

**One font family in one font file.**

- when was the technology launched? who’s behind it?
- why use variable fonts? what are the advantages?
- which applications currently support variable fonts?

> - [Introducing OpenType Variable Fonts (John Hudson)](http://medium.com/@tiro/https-medium-com-tiro-introducing-opentype-variable-fonts-12ba6cd2369)
> - [Special OpenType Session (ATypI Warsaw, 2016)](http://www.youtube.com/watch?v=6kizDePhcFU)
{: .seealso }

> - add glossary entries for {% glossary master %}, {% glossary designspace %}, {% glossary interpolation %} – see `_data/glossary.yml`
{: .todo }

## Variable fonts vs. interpolation, mutator math

*Explain the relationship between variable fonts, interpolation and mutator math/superpolation.*

Variable fonts brings interpolation from production tools to the end-user font format.

Type designers are familiar with using interpolation in the font editor, for example to create families with several weights and widths. We are also familiar with creating complex interpolation spaces using [Superpolator], or scripting them with [mutatorMath].

> - {% internallink "working-with-interpolation" %}
{: .seealso }

Some differences between normal interpolation and mutator math:

- interpolation: full compatibility required - same character set, same kerning pairs, same groups
- mutator math: full compatibility not required

> The Batch extension solves most of these issues, by inserting ‘repair mutators’
{: .note }

## How variable fonts work

*Explain the most fundamental aspects of variable font technology.*

Neutral + deltas
: Variable fonts have a default or neutral master. All other masters in the {% glossary designspace %} have to obey this default master. Example: If the glyph `A` has 39 points in contour 1, then the same glyph `A` must have 39 points in contour 1 *in all the masters*.

Missing glyphs
: Not every master must have all glyphs. If a glyph is missing, it will be calculated via interpolation. If a glyph is present only in one master, it will have no variations.

Discontinuous variation
: This is about the famous `dollar.nostroke` vs `dollar.withstroke`

## Examples of variable fonts

*Collect some good examples of variable fonts to give an overview of what’s possible.*

- [Axis Praxis](http://axis-praxis.org/)
- ...

---
layout: page
title: Giving feedback on the documentation
tags:
  - documentation
level: intermediate
draft: true
---

**You can help to make the RoboFont Docs better!**

All forms of feedback all welcome:

- corrections or additions to content
- corrections of typos or language mistakes (the editors are not native English speakers!)
- any type of comments, suggestions, requests etc.

## Feedback channels

Please use one of the communication channels below to get in touch:

[RoboFont Forums](http://forum.robofont.com/t/documentation)
: for questions and discussions

[GitLab Issues](http://gitlab.com/typemytype/robofont_com/issues)
: for bug reports, feature requests, etc.

## Making changes directly

If you find an error and you are familiar with Git, you are welcome to fix the error and submit a merge request.

See {% internallink "editing-the-docs" %} for instructions.

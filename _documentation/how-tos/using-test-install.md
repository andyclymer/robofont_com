---
layout: page
title: Using Test Install to proof fonts
tags:
  - proofing
  - OpenType
level: intermediate
draft: true
---

RoboFont can install a font directly in the OS. This font will be directly available for use in any other application.

The font will be installed only for the current user and will be deactivated when the UFO document in RoboFont is closed, when the user quits RoboFont or when the user logs out.

Test install will always generate an .otf file. There are some settings you can alter in the Preferences like decompose, auto hint and remove overlap.

> - [RoboFont Test Install (vimeo)](http://vimeo.com/33480815)
{: .seealso }

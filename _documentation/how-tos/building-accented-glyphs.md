---
layout: page
title: Building accented glyphs
tags:
  - accents
level: intermediate
draft: true
---

<!--
1.  how components work
2.  using anchors to position accent on base glyph
3.  glyph construction dictionary
4.  building accented glyphs with a script
5.  extensions to build and deal with accented glyphs
-->

## Different ways to build accented glyphs

- {% internallink "adding-and-removing-glyphs#glyph-construction-rules" text="building accented glyphs with the Add Glyphs sheet" %}
- {% internallink "building-glyphs-with-glyph-construction" %}
- {% internallink "building-accented-glyphs-with-script" %}

## Extensions for working with accented glyphs

- Adjust Anchors
- Anchor Overlay Tool

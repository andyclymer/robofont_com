---
layout: page
title: Creating designspace files
tags:
  - variable fonts
  - interpolation
  - designspace
level: intermediate
draft: true
---

<!--
* Table of Contents
{:toc}
-->

The `.designspace` file is where all required information about the variable font is defined:

- axes (name, range)
- masters and their positions
- instances
- optional extra information: rules?

There are different ways to create `.designspace` files:

- using [Superpolator](http://superpolator.com/)
- using the [DesignSpaceEditor](http://github.com/LettError/designSpaceRoboFontExtension) extension)
- using [a script](http://github.com/LettError/designSpaceDocument/blob/master/scripting.md)

---
layout: page
title: Generating fonts
tags:
  - OpenType
  - scripting
level: beginner
draft: false
---

* Table of Contents
{:toc}

## Supported output formats

RoboFont can generate fonts in the following binary formats:

OpenType CFF
: PostScript-flavored OpenType fonts, typically with a `.otf` file extension. Glyph shapes are described with cubic beziers.

OpenType TTF
: TrueType-flavored OpenType fonts, typically with a `.ttf` file extension. Glyph shapes are described with quadratic beziers.

PostScript
: Old-school PostScript fonts (cubic beziers). Font files have a `.pfa` file extension.

> Other font formats are supported by means of extensions or external libraries:
>
> Variable fonts
> : OpenType variable fonts can be generated using the [Batch] extension.
>
> Color fonts
> : Color fonts in SVG, COLR/CPAL and sbix formats can be generated using the [RoboChrome] extension.
>
> VFB fonts
> : If [vfb2ufo] is installed, fonts in FontLab’s `.vfb` format can be generated and opened with RoboFont.
>
> Webfonts
> : Webfonts in all formats – WOFF, WOFF2, EOT, SVG – can also be generated with the [Batch] extension.
{: .note }

[RoboChrome]: http://github.com/jenskutilek/RoboChrome
[vfb2ufo]: http://blog.fontlab.com/font-utility/vfb2ufo/
[Batch]: http://github.com/typemytype/batchRoboFontExtension

## Generating fonts via the menu

To generate the current font, choose *File > Generate Font* from the {% internallink 'workspace/application-menu' %} to open the *Generate Font* sheet:

{% image how-tos/generating-fonts_sheet.png %}

### Options

<table>
  <thead>
    <tr>
        <th width='35%'>option</th>
        <th width='65%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Save As</td>
      <td>The name of the output font file.</td>
    </tr>
    <tr>
      <td>Tags</td>
      <td>Tags to be assigned to the generated file. (optional)</td>
    </tr>
    <tr>
      <td>Where</td>
      <td>Folder where the file should be generated.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>layer</td>
      <td>Choose a UFO layer as the source for the generated font.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>decompose</td>
      <td>Convert all components into contours.</td>
    </tr>
    <tr>
      <td>remove overlap</td>
      <td>Remove overlaps in the outlines of all glyphs.</td>
    </tr>
    <tr>
      <td>autohint</td>
      <td>Apply the AFDKO’s autohint program to the font.</td>
    </tr>
    <tr>
      <td>release mode</td>
      <td>Set release mode. This turns on subroutinization, applies the <code>GlyphOrderAndAliasDB</code> file, and removes "Development" from the name table Version string.
</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>format</td>
      <td>Choose a format for the generated font.</td>
    </tr>
    <tr>
      <td>use MacRoman as start of the glyph order</td>
      <td>Use the MacRoman character set as the first glyphs in the font.</td>
    </tr>
  </tbody>
</table>

## Generating fonts with a script

Use the `font.generate` method to generate fonts:

```python
f = CurrentFont()

formats = {
  'OpenType-CFF (.otf)' : 'otfcff',
  'OpenType-TTF (.ttf)' : 'otfttf',
  'PostScript (.pfa)'   : 'pctype1ascii',
}

for format in formats.keys():
    print('Generating %s font...' % format)
    print(f.generate(formats[format]))
    print
```

> - make sure this works in RF3!
{: .todo }

> - {% internallink "using-test-install" %}
> - {% internallink "using-the-batch-extension" %}
{: .seealso }

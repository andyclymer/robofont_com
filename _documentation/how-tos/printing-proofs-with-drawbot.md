---
layout: page
title: Printing proofs with DrawBot
tags:
  - proofing
  - drawBot
level: intermediate
draft: true
---

* printing from UFO vs. printing from OTF
* DrawBot RoboFont extension

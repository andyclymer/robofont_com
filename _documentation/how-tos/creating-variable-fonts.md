---
layout: page
title: Creating variable fonts
tags:
  - variable fonts
  - interpolation
  - designspace
level: advanced
draft: true
---

* Table of Contents
{:toc}

<!--
*Step-by-step guide for beginners on creating a variable font.*
-->

> - create a boilerplate variable font? see {% internallink "boilerplate-extension" %}
{: .todo }

## Designing the design space

- define which features of the design will be variable
- how many axes? how many masters?

## Creating compatible masters

- design the masters as separate UFOs
- {% internallink "preparing-for-interpolation" text="make all masters compatible" %}

## Creating a designspace file

The `.designspace` file is where all required information about the variable font is defined. See {% internallink "creating-designspace-files" %} for a step-by-step guide.

## Generating the variable font

> Step by step quick OTVar Demo:
>
> - font1 glyphA glyphB
> - font2 glyphA
> - dsd
> - batch
{: .note }

options:

- Batch extension
- fontTools (make simple example script)
- fontMake?
- AFDKO?

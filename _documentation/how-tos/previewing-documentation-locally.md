---
layout: page
title: Previewing the documentation locally
tags:
  - documentation
level: intermediate
---

* Table of Contents
{:toc}

The RoboFont documentation is built with [Jekyll], a site generator which converts [Markdown] sources and some other files in into a static website.

Jekyll comes with a built-in development server which allows you to preview how the generated site will look like in your browser, locally.

[Markdown]: http://kramdown.gettalong.org/
[Jekyll]: http://jekyllrb.com/

## Installing Jekyll

If you haven’t used Jekyll before, you’ll need to install it first. Just follow the instructions [here](http://jekyllrb.com/docs/installation/).

## Running the local server

> To preview the RoboFont documentation locally, you’ll need to have a local version of the [robofont_com] repository. Follow steps 1 & 2 of {% internallink "how-tos/editing-the-docs#editing-content-with-git" %} to fork the repository and make a clone in your machine.
{: .note }

[robofont_com]: http://gitlab.com/typemytype/robofont_com/

To start the local server, open the Terminal, go to the repository’s folder, and tell Jekyll to build the site on the preview server:

```text
cd path/to/robofont_com/
bundle exec jekyll serve
```

The output in Terminal will be something like this:

```console
Configuration file: /_code/robofont_com/_config.yml
Configuration file: /_code/robofont_com/_config.yml
            Source: /_code/robofont_com
       Destination: /_code/robofont_com/_site
 Incremental build: enabled
      Generating...
                    done in 6.02 seconds.
 Auto-regeneration: enabled for '/_code/robofont_com'
Configuration file: /_code/robofont_com/_config.yml
    Server address: http://127.0.0.1:4000/robofont_com/
  Server running... press ctrl-c to stop.
```

Your website can now be viewed in your favorite browser in this URL:

[http://127.0.0.1:4000/robofont_com/](http://127.0.0.1:4000/robofont_com/)

To stop the local server, press Ctrl + c.

## Editing and previewing workflow

1. In your browser, go to the documentation page you would like to edit.
2. Open the markdown source for that page in your code editor.
3. Make changes to the source file, and save it.
4. Wait a few seconds, go to your browser and reload the page.
5. View changes in the updated site, think about more improvements.
6. Go back to 3 and repeat.

> Use `bundle exec jekyll serve --incremental` during development to rebuild only the modified files. This makes the editing & previewing workflow faster.
{: .tip }

---
layout: page
title: Using RoboFont from the command line
tags:
  - scripting
level: advanced
draft: true
---

RoboFont can be used from the command line.

Install the command line tool in the [Preferences](#).

The command line tool has a very simple API:

```text
roboFont [-h] [-p] [-c] [-o]
```

<table>
    <tr>
        <td><code>--help -h</code></td>
        <td>show options</td>
    </tr>
    <tr>
        <td><code>--pythonpath -p &lt;path&gt; &lt;path&gt; ...</code></td>
        <td><code>.py</code> files path(s) to run inside RoboFont</td>
    </tr>
    <tr>
        <td><code>--pythoncode -c "print(5*5")</code></td>
        <td>Python code to run inside RoboFont</td>
    </tr>
    <tr>
        <td><code>--openfile -o &lt;path&gt;</code></td>
        <td>files to open with RoboFont</td>
    </tr>
</table>

> [RoboFont shell command in action (Vimeo)](http://vimeo.com/42008861)
{: .seealso }

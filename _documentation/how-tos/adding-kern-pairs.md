---
layout: page
title: Adding kern pairs to a font
tags:
  - kerning
level: beginner
draft: true
---

* Table of Contents
{:toc}

## Introduction

Kerning is an optimization of the whitespace. It’s part of the visual design of a font.

> Before kerning begins, it is important to space the font really well. Don’t use kerning to fix spacing problems!
{: .tip }

## Kerning workflow

1. define kerning groups
2. create kerning pairs (using groups)
3. iterate

## Defining kerning groups

{% image workspace/groups-editor_kerning-groups.png %}

## Creating kerning pairs

{% image workspace/kern-center_groups-edit.png %}

## Proofing & tweaking

proof, edit groups, edit values, etc.

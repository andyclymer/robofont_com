---
layout: page
title: Working with fonts with a large amount of glyphs
tags:
  - UFO
  - character set
level: intermediate
draft: true
---

* Table of Contents
{:toc}

When opening a font in the interface, RoboFont needs to load data from all glyphs in the font – in order to render the glyph cells in the {% internallink 'workspace/font-overview' %}, and to build the font’s unicode map (used when typing text in the {% internallink 'workspace/space-center' %}, for example).

In the {% internallink 'introduction/the-ufo-format' text='UFO format' %}, each glyph is stored as a separate [.glif](http://unifiedfontobject.org/versions/ufo3/glyphs/glif/) file inside the UFO package; the more glyphs in a font, the more individual files need to be parsed. Because of this, fonts with a large amount of glyphs can have a negative impact on the speed and performance of RoboFont.

To make the opening of large fonts faster, RoboFont includes a preference setting to control the maximum amount of glyphs which are loaded when a font is opened. The remaining glyphs can be loaded later, if required.

*This page gives an overview of all settings and dialogs related to working with fonts with a large amount of glyphs in RoboFont.*

## Setting the maximum amount of glyphs

The maximum amount of glyphs which are loaded when a font is opened is controlled by the setting *Max glyph count to read instantly from disk* in the {% internallink "workspace/preferences-window/character-set" %}. The default value is 2000 glyphs.

{% image how-tos/working-with-large-fonts_max-glyph-count-preference.png %}

> This preference is stored internally as `loadFontGlyphLimit`, and can also be edited with the {% internallink "workspace/preferences-editor" %}.
{: .note }

## Opening Large Fonts warning

When opening a font which contains more glyphs than the limit defined in the *Max glyph count* preference, a pop-up window with a warning will appear:

{% image how-tos/working-with-large-fonts_opening-large-fonts-popup.png %}

Click on the *OK* button to close the dialog. Select the checkbox if you don’t want RoboFont to show this message again.

> If you have previously selected the *Don’t show this message again* checkbox, and wish to undo this choice: use the {% internallink "workspace/preferences-editor" %} to set the `warningsLevel` setting to `always`.
{: .note }

## ‘Load All Glyphs’ button

When a font containing more glyphs than the *Max glyph count* limit is opened, a small button labeled *Load All Glyphs* will appear at the bottom left of the {% internallink 'workspace/font-overview' %} window. Pressing this button will force RoboFont to load data from all glyphs in the font.

{% image workspace/font-overview_load-all-glyphs.png %}

## Simple font window

An alternative to loading just a limited amount of glyphs from a large font is loading all glyphs using a simplified font window. Instead of the usual {% internallink 'workspace/font-overview' %} interface with glyph cells, we can use Python to create a simpler (and faster) font window with only a plain list of glyph names. Double-clicking a glyph name opens a {% internallink 'workspace/glyph-editor' %} for that glyph.

{% showcode how-tos/simpleFontWindow.py %}

> - redo last screenshots
> - fix display bug in Character Set Preferences
{: .todo }

---
layout: page
title: Adding and removing glyphs
tags:
  - character set
level: beginner
---

* Table of Contents
{:toc}

## Adding glyphs

Choose *Font > Add Glyphs* from the {% internallink "workspace/application-menu" %} to open the Add Glyphs sheet:

{% image how-tos/adding-and-removing-glyphs_add-glyphs-sheet.png %}

In the main text area, fill in the names of the glyphs you would like to create, as a space-separated list.

### Options

<table>
  <thead>
    <tr>
      <th width="35%">option</th>
      <th width="65%">description</th>
    </tr>
  </thead>
  <tbody>

    <tr>
      <td>Import glyph names from…</td>
      <td>Import glyph names from saved character sets (see <a href="../../workspace/preferences-window/character-set">Character Set Preferences</a>) or from the current font.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Add unicode</td>
      <td>Try to set the unicode values for the given glyph names automatically.</td>
    </tr>
    <tr>
      <td>Sort Font</td>
      <td>Sort the glyphs in the font alphabetically.</td>
    </tr>
    <tr>
      <td>Overwrite Glyphs</td>
      <td>Overwrite glyphs if they already exist in the font.</td>
    </tr>
    <tr>
      <td>Add As Template Glyphs</td>
      <td>Add glyphs as template glyphs only. If this option is unchecked, empty new glyphs will be created instead.</td>
    </tr>
  <tr>
      <td>Mark Color</td>
      <td>Click on the color swatch to open a color picker, and choose a mark color for the new glyphs.</td>
    </tr>
  </tbody>
</table>

### Glyph construction rules

Glyph names support basic glyph construction rules which provide additional functions for creating new glyphs.

Building new glyphs with one or more components:

```text
uni0430=a
aacute=a+acute
aringacute=a+ring+acute
```

Aligning components using anchors:

```text
aacute=a+acute@top
```

> For this example to work, the base glyph `a` must have an anchor named `top`, and the accent glyph `acute` must have an anchor named `_top`.
{: .note }

Assigning a unicode value to a glyph name:

```text
aacute=a+acute@top|00E0
```

> The simple glyph construction rules provided by the Add Glyphs sheet have been expanded into the more powerful [Glyph Construction] language.
>
> See also {% internallink "building-glyphs-with-glyph-construction" %}.
{: .seealso }

[Glyph Construction]: http://github.com/typemytype/GlyphConstruction

## Removing glyphs

Select the glyphs which you would like to delete from the font in the Font Overview, and press the backspace key.

To remove template glyphs, use ⌥ + backspace.

> When a glyph is deleted from the font, it is also automatically removed from `font.glyphOrder`.
{: .note }

Depending on your settings in {% internallink "workspace/preferences-window/character-set" %}, the deleted glyphs will also be removed from groups and/or kerning pairs.

{% image workspace/preferences_character-set_delete-glyph.png %}

> Removing glyphs from groups and/or kerning applies only when deleting glyphs manually from the Font Overview, by selecting and pressing the backspace key.
>
> Glyphs which are deleted via scripts are **not** removed from groups or kerning, independent of user settings.
{: .note }

> Deleted glyphs are **not** removed automatically from OpenType features.
>
> It is important to remove deleted glyphs from features code before generating OpenType fonts – if the OpenType compiler encounters a glyph which is not in the font, it will raise an error and the font will not be generated.
{: .warning }

## Adding and removing glyphs with Python

Of course, it is also possible to add and remove glyphs to/from a font with Python.

### Adding glyphs

Here’s an example showing how to add glyphs with code, including a check to avoid overwriting existing glyphs:

```python
f = CurrentFont()

# add a new glyph to the font
# if the glyph already exists, it will be overwritten

f.newGlyph('hello')

# if you don't want to overwrite a glyph,
# first check if it already exists in the font

newGlyphName = 'A'

if newGlyphName not in f.keys():
    f.newGlyph(newGlyphName)
else:
    print("glyph '%s' already exists in the font" % newGlyphName)

# add several glyphs at once with a loop

newGlyphNames = ['test1', 'test2', 'test3']

for glyphName in newGlyphNames:
    f.newGlyph(glyphName)
```

### Removing glyphs

And here an example showing how to remove glyphs with code, including a check to avoid errors if a glyph is not included in the font:

```python
f = CurrentFont()

# before removing a glyph,
# make sure it actually exists in the font

glyphName = 'hello'

if glyphName in f.keys():
    f.removeGlyph(glyphName)
else:
    print("font does not contain a glyph named '%s'" % glyphName)

# otherwise, and error will be raised when trying to
# remove a glyph which is not included in the font

f.removeGlyph('spam')
```

```console
Traceback (most recent call last):
  File "<untitled>", line 16, in <module>
  File "/Applications/RoboFont2.app/Contents/Resources/lib/python2.7/fontParts/base/layer.py", line 191, in removeGlyph
FontPartsError: No glyph with the name 'spam' exists.
```

### Removing glyphs from groups

```python
f = CurrentFont()

# glyph to be removed
glyphName = 'd'

# iterate over all groups in the font
for groupName in f.groups.keys():

    # get the group
    group = f.groups[groupName]

    # if glyph is in the group, remove it
    if glyphName in group:
        print('removing %s from group %s...' % (glyphName, groupName))
        group.remove(glyphName)
```

### Removing glyphs from kerning

```python
f = CurrentFont()

# the glyph to be removed
glyphName = 'A'

# iterate over all kerning pairs in the font
for kerningPair in f.kerning.keys():

    # if glyph is in the kerning pair, remove it
    if glyphName in kerningPair:
        print('removing kerning pair (%s, %s)...' % kerningPair)
        del f.kerning[kerningPair]
```

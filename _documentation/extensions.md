---
layout: page
title: Extensions
tree:
    - extensions-introduction
    - getting-extensions
    - installing-extensions
    - installing-extensions-mechanic
    - using-extensions
    - creating-extensions
level: beginner
slideShows:
  extensions:
    - image: extensions/overview/outliner.png
      caption: |
        [Outliner](http://github.com/typemytype/outlinerRoboFontExtension) by Frederik Berlaen
    - image: extensions/overview/word-o-mat.png
      caption: |
        [word-o-mat](http://github.com/ninastoessinger/word-o-mat) by Nina Stössinger
    - image: extensions/overview/ramsay-st.png
      caption: |
        [Ramsay St](http://github.com/typemytype/ramsayStreetRoboFontExtension) by Frederik Berlaen
    - image: extensions/overview/GlyphBrowser.png
      caption: |
        [GlyphBrowser](http://github.com/LettError/glyphBrowser) by Erik van Blokland
    - image: extensions/overview/italic-bowtie.png
      caption: |
        [Italic Bowtie](http://github.com/FontBureau/fbOpenTools) by Cyrus Highsmith
    - image: extensions/overview/GlyphConstruction.png
      caption: |
        [Glyph Construction](http://github.com/typemytype/GlyphConstruction) by Frederik Berlaen
    - image: extensions/overview/glifViewer.png
      caption: |
        [Glif Viewer](http://github.com/typemytype/glifViewerRoboFontExtension) by Frederik Berlaen
    - image: extensions/overview/GlyphNanny.png
      caption: |
        [Glyph Nanny](http://github.com/typesupply/glyph-nanny) by Tal Leming
    - image: extensions/overview/OverlayUFOs.png
      caption: |
        [Overlay UFOs](http://github.com/FontBureau/fbOpenTools) by David Jonathan Ross
    - image: extensions/overview/showSparks.png
      caption: |
        [Show Sparks](http://github.com/LettError/showSparks) by Erik van Blokland
    - image: extensions/overview/RoboREPL2.png
      caption: |
        [RoboREPL](http://github.com/typesupply/roborepl) by Tal Leming
    - image: extensions/overview/Slanter.png
      caption: |
        [Slanter](http://github.com/typemytype/slanterRoboFontExtension) by Frederik Berlaen
  autoPlay: true
  height: 400
---

<!--
**How to find, install and use RoboFont extensions.**
-->

{% include slideshows items=page.slideShows.extensions %}

{% comment %}
to-do: add option to choose between long and short titles in tree too
{% tree page.url levels=1 %}
{% endcomment %}

- {% internallink "extensions-introduction" %}
- {% internallink "getting-extensions" %}
- [Installing extensions manually](installing-extensions/)
- {% internallink "installing-extensions-mechanic" %}
- {% internallink "managing-extension-streams" %}
- {% internallink "using-extensions" %}
- {% internallink "creating-extensions" %}

> - {% internallink "introducing-mechanic-2" %}
> - [Building extensions](../building-tools/extensions/)
{: .seealso }

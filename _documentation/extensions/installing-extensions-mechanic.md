---
layout: page
title: Installing extensions with Mechanic 2
treeTitle: Installing with Mechanic 2
tags:
  - extensions
  - Mechanic
level: beginner
---

* Table of Contents
{:toc}

**Mechanic is a RoboFont extension which makes it easier to find, download, install/uninstall, and update other RoboFont extensions.**

> - {% internallink 'extensions/introducing-mechanic-2' %}
{: .seealso }

## Installing Mechanic 2

To use Mechanic you will need to install it first: download the [Mechanic 2 extension] from GitHub, and [install it manually](../installing-extensions).

Once installed, Mechanic 2 will appear in RoboFont’s *Extensions* menu.

## Finding extensions

Go to the *Extensions* menu and open the Mechanic 2 window. Mechanic will pull information from the registered extension streams, and display a list of available extensions.

{% image extensions/installing-extensions-mechanic_extensions-list.png %}

> - paid extensions from the [Extension Store] are listed first
> - installed extensions are marked with a gray circle
> - new in Mechanic 2: extensions may include an icon
{: .note }

### Using the search bar

The search bar can be used to search in extension names, authors and tags.

The following special search words are supported:

`?installed?`
: shows all installed extensions

`?not_installed?`
: shows all extensions which are *not* installed

`?update?`
: shows all extensions for which updates are available

## Installing extensions

Mechanic 2 comes with [Mechanic][Mechanic website] and [Extension Store] as default extension streams. Other extension streams and single extension items can be added using the *Settings* sheet.

> - {% internallink 'extensions/managing-extension-streams' %}
{: .seealso }

When installing an extension, Mechanic will fetch the extension’s source code, and copy it to the appropriate folder in your system:

```text
~/Library/Application Support/RoboFont/plugins/
```

Installing free & open-source extensions
: To install an open-source extension, simply select it from the list, and click on the *Install* button.

Installing commercial extensions
: To install a paid extension you’ll need to buy it first. Select the extension from the list, and click the *Purchase* button – you will be taken a payment page in the Extension Store. After the payment process is completed, the extension will be downloaded and installed.

That’s it! The extension is now available and ready to use.

## Uninstalling extensions

To uninstall an extension, select it from the list, and click on the *Uninstall* button.

## Updating extensions

Use the *Check for Updates* button to check if there are any updates available for the installed extensions.

To update an extension, click on the *Install* button; when the confirmation dialog appears, choose *Re-Install*.

[Mechanic 2 extension]: http://github.com/robofont-mechanic/mechanic-2
[Mechanic website]: http://robofontmechanic.com/
[Extension Store]: http://extensionstore.robofont.com/

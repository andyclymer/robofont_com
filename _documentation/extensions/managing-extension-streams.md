---
layout: page
title: Managing extension streams in Mechanic 2
tags:
  - extensions
  - Mechanic
level: intermediate
---

* Table of Contents
{:toc}

## Introduction

An *extension stream* is a collection of metadata about RoboFont extensions. Multiple extension servers provide extension streams in `.json` format. The Mechanic 2 extension reads data from linked streams, and builds a single list of supported extensions.

Mechanic 2 comes with two pre-configured extension streams: [Mechanic][Mechanic website] and [Extension Store]. Other streams and individual extensions can be added with the *Settings* sheet.

## Adding extension streams

{% image extensions/managing-extension-streams_extension-streams.png %}

Use the +/- buttons to add/remove extension streams from the list.

Local extension streams are also supported:

```console
localhost:8888/mechanic-2-server/api/extensions.json
```

> Support for multiple extension streams opens some interesting possibilities, such as curated lists of extensions or custom commercial stores…
{: .tip }

## Adding single extension items

Mechanic 2 also supports adding individual extensions to the stream.

{% image extensions/managing-extension-streams_single-items.png %}

Use the +/- buttons to add/remove `.mechanic` files with data for single extensions.

## Extension stream format

The extension stream is a simple `.json` file with a list of extension metadata items and a date (when the list was last updated).

Each extension metadata item must contain the following attributes:

<table>
  <tr>
    <th width='30%'>key</th>
    <th width='70%'>description</th>
  </tr>
  <tr>
    <td><code>extensionName</code></td>
    <td>Name of the extension.</td>
  </tr>
  <tr>
    <td><code>repository</code></td>
    <td>URL of the Git repository containing the extension.</td>
  </tr>
  <tr>
    <td><code>extensionPath</code></td>
    <td>Path to the <code>.roboFontExt</code> file inside the repository.</td>
  </tr>
  <tr>
    <td><code>description</code></td>
    <td>A short description of the extension.</td>
  </tr>
  <tr>
    <td><code>developer</code></td>
    <td>Name of the developer.</td>
  </tr>
  <tr>
    <td><code>developerURL</code></td>
    <td>Site URL of the developer.</td>
  </tr>
  <tr>
    <td><code>tags</code></td>
    <td>A list of tags related to the extension.</td>
  </tr>
</table>

Here is an example extension stream in `.json` format:

```json
{
  "lastUpdate" : "2018-06-08 21:21",
  "extensions" : [
    {
      "extensionName": "myExtension",
      "repository" :  "http://github.com/robodocs/rf-extension-boilerplate",
      "extensionPath" : "build/myExtension.roboFontExt",
      "description" : "A boilerplate extension which serves as starting point for creating your own extensions.",
      "developer" : "RoboDocs",
      "developerURL" : "https://github.com/roboDocs",
      "tags" : ["demo"]
    }
  ]
}
```

## Extension item format

Individual extension metadata is stored as `yaml` data in `.mechanic` files. All the keys used by the [extension stream format](#extension-stream-format) are required, plus the following:

<table>
  <tr>
    <th width='30%'>key</th>
    <th width='70%'>description</th>
  </tr>
  <tr>
    <td><code>infoPath</code></td>
    <td>Path to the <code>info.plist</code> file inside the extension package.</td>
  </tr>
  <tr>
    <td><code>zipPath</code></td>
    <td>Path to the <code>.zip</code> file inside the Git repository.</td>
  </tr>
</table>

Here is an example `.mechanic` file:

```yaml
extensionName: PixelChecker
repository: https://gitlab.com/typemytype/tmt-robofont-extensions
extensionPath: PixelShadow/PixelChecker.roboFontExt
description: Adding pixel in a chekers grid to any glyph.
infoPath: https://gitlab.com/typemytype/tmt-robofont-extensions/raw/master/PixelShadow/PixelChecker.roboFontExt/info.plist?private_token=n3gnqfjr5wpoCg9AY8Jq
zipPath: https://gitlab.com/typemytype/tmt-robofont-extensions/-/archive/master/tmt-robofont-extensions-master.zip?private_token=n3gnqfjr5wpoCg9AY8Jq
tags: [ pixel ]
developer: Frederik Berlaen
developerURL: http://typemytype.com
```

> Extensions in private repositories are allowed if a `private_token` is provided (see the example).
{: .tip }

[Mechanic 2 extension]: http://github.com/robofont-mechanic/mechanic-2
[Mechanic website]: http://robofontmechanic.com/
[Extension Store]: http://extensionstore.robofont.com/

---
layout: page
title: Where to find installed extensions
tag:
  - extensions
level: beginner
---

* Table of Contents
{:toc}

**So you have installed an extension, either manually or via Mechanic. And now you want to use it. But wait, where can I find it?!**

How an extension is activated and used depends on each extension and the type of tool it contains – look at the extension’s documentation for usage instructions.

*This page gives an overview of the different ways an extension can make itself accessible in RoboFont.*

## Extensions menu

This is the standard mechanism for making extensions available, and by far the most common case. The extension adds an entry to the *Extensions* menu, with or without a submenu. From there, one or more dialogs can be opened.

{% image extensions/using-extensions_extensions-menu.png caption='the Extensions menu' %}

Examples: [RoboREPL], [FeaturePreview], [DrawBot]

## Tools menu

Some extensions contain interactive tools which are used when editing glyphs. These extensions make themselves available by adding an entry to the toolbar at the top of the {% internallink 'glyph-editor' %}.

{% image extensions/using-extensions_custom-tools.png caption='custom tools: Scaling Edit Tool (selected), Bounding Tool, Pixel Tool' %}

Examples: [Pixel Tool], [Scaling Edit Tool]

## New entries in existing menus

Some extensions add entries to other {% internallink "workspace/application-menu" %} items instead of *Extensions*. This makes sense for extensions which add functionality to objects which are already present in the main application menu, such as *Font*, *Glyph* or *Window*.

{% image extensions/using-extensions_existing-menus.png caption='Batch appears in the File menu' %}

Examples: [Arrange Windows], [Batch]

## New submenu in the main application menu

While not very common, this is a useful solution for extensions which require lots of menu space. Instead of cluttering the *Extensions* menu, the extension can add its own menu to the main application menu.

{% image extensions/using-extensions_application-menu.png %}

Examples: [hTools2]

## No visible UI control

Finally, some extensions don’t add any menu item or tool. They do their job silently by adding new layers of information to existing views.

{% image extensions/using-extensions_show-info.png %}

Examples: [Show Character Info], [Show Mouse Coordinates], [ShowDist]

[RoboREPL]: http://github.com/typesupply/roborepl
[FeaturePreview]: http://github.com/typemytype/featurePreviewRoboFontExtension
[DrawBot]: http://github.com/typemytype/drawBotRoboFontExtension
[Pixel Tool]: http://github.com/typemytype/pixelToolRoboFontExtension
[Scaling Edit Tool]: http://github.com/klaavo/scalingEditTool
[Arrange Windows]: http://github.com/typemytype/arrangeWindowsRoboFontExtension
[Batch]: http://github.com/typemytype/batchRoboFontExtension
[hTools2]: http://github.com/gferreira/hTools2_extension
[Show Character Info]: http://github.com/FontBureau/fbOpenTools
[Show Mouse Coordinates]: http://github.com/FontBureau/fbOpenTools
[ShowDist]: http://github.com/frankrolf/showDist

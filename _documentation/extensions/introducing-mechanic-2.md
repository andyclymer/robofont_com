---
layout: page
title: Introducing Mechanic 2
tags:
  - extensions
  - Mechanic
---

* Table of Contents
{:toc}

## Background

Mechanic – the RoboFont extension to manage RoboFont extensions – was conceived and developed by Jack Jennings. [Released in 2015][Mechanic Blog], it quickly became an essential tool for many RoboFont users. It established a bridge between extension developers and designers, and stimulated the creation of more and better extensions. Today Mechanic hosts nearly 100 open-source extensions by 30 different authors.

### Why Mechanic 2

With the development of RoboFont 3, Mechanic was initially ported to Python 3 too – but this update introduced bugs which turned out to be really difficult to solve. Around the same time, the [Extension Store] was developed and released, offering similar extension management functions – but built using a different set of tools.

The challenge to update Mechanic for RoboFont 3 became a opportunity to join forces and build the next generation of Mechanic from scratch. Mechanic 2 keeps the essential functionality and look’n’feel of Mechanic, but both website and extension were completely rewritten using the same infra-structure as the Extension Store.

The result is an all new Mechanic experience which looks and feels familiar to long-time Mechanic users, works smoothly with RoboFont 3 – and has [some new tricks](#mechanic-extension) to offer.

## Mechanic architecture

Mechanic consists of two components:

Mechanic website
: ^
  - a database of open-source RoboFont extensions
  - a web-based interface to browse through extension data

Mechanic extension
: An extension to find, download, install/uninstall and update extensions.

## Differences between Mechanic and Mechanic 2

<table>
  <tr>
    <th width='50%'>Mechanic</th>
    <th width='50%'>Mechanic 2</th>
  </tr>
  <tr>
    <td>server written in Ruby</td>
    <td>static website written with Jekyll</td>
  </tr>
  <tr>
    <td>extension metadata is stored in <code>.plist</code> files inside the extensions</td>
    <td>extension metadata is stored in <code>.yaml</code> files inside the website</td>
  </tr>
  <tr>
    <td>extensions are registered from within RoboFont using the Mechanic extension</td>
    <td>extensions are registered by forking the Mechanic 2 website, adding extension data, and submitting a pull request</td>
  </tr>
  <tr>
    <td>red theme</td>
    <td>blue theme</td>
  </tr>
  <tr>
    <td>developed and maintained by Jack Jennings</td>
    <td>developed and maintained by <a href='http://github.com/orgs/robofont-mechanic/people'>Mechanic Developers™</a></td>
  </tr>
</table>

## New features in Mechanic 2

### Mechanic website

On the surface, the new [Mechanic 2 website] looks and feels the same – only a few UI changes were introduced:

- the list of extensions can now be filtered by tags and/or developers
- a direct link to the zip download is shown next to the link to the repository (only for extensions hosted on GitHub)
- a hover effect highlights the current row

### Mechanic extension

The new [Mechanic 2 extension] introduces one powerful new feature: **multiple extension streams**.

An *extension stream* is a collection of metadata about RoboFont extensions. Different *extension servers* provide extension streams in `.json` format.
The Mechanic 2 extension reads data from the linked streams, and builds a list of supported extensions.

Mechanic 2 comes with two pre-configured extension streams: Mechanic and Extension Store.

**Commercial extensions from the Extension Store can now be installed and managed alongside free & open-source extensions from Mechanic.**

> The Extension Store extension has been deprecated.
{: .note }

Mechanic 2 also supports `.mechanic` files with metadata about individual extensions. Thanks to this feature, **developers can now use Mechanic 2 to deliver updates of custom extensions to clients**.

> Read more about Mechanic 2 in the updated documentation pages:
> - {% internallink 'extensions/installing-extensions-mechanic' %}
> - {% internallink 'building-tools/extensions/publishing-extensions' %}
{: .seealso }

## Moving forward

Mechanic 2 is now live and replaces Mechanic as the official extension manager for RoboFont. The future is bright for developers and users of RoboFont 3. Enjoy!

**Huge thanks to Jack Jennings for creating Mechanic, and for making Mechanic 2 possible!!**

[Mechanic 2 website]: http://robofontmechanic.com/
[Mechanic 2 extension]: http://github.com/robofont-mechanic/mechanic-2
[Mechanic Blog]: http://web.archive.org/web/20170915150601/http://blog.robofontmechanic.com:80/
[Extension Store]: http://extensionstore.robofont.com/

font = CurrentFont()
layer = font.getLayer(font.defaultLayer)
layer.renameGlyph(oldName, newName, renameComponents=True, renameGroups=True, renameKerning=True, renameGlyphOrder=True)
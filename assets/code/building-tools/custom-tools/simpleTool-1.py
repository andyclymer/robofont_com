from mojo.events import BaseEventTool, installTool, uninstallTool
from mojo.drawingTools import *

class MyTool(BaseEventTool):

    def setup(self):
        self.position = None
        self.size = 20
        self.shape = oval
        self.color = 1, 0, 0, 0.5

    def mouseDown(self, point, clickCount):
        # self.position = point
        self.size *= 4

    def mouseDragged(self, point, delta):
        self.position = point

    def mouseUp(self, point):
        # self.position = point
        self.size *= 0.25

    def mouseMoved(self, point):
        self.position = point
        self.refreshView()

    def modifiersChanged(self):
        # get modifier keys
        modifiers = self.getModifiers()

        # define shape based on 'shift' key:
        # > if 'shift' is pressed, shape is a rectangle
        if modifiers['shiftDown']:
            self.shape = rect
        # > otherwise, shape is an oval
        else:
            self.shape = oval

        # change color based on 'option' key:
        # > if 'option' is pressed, color is blue
        if modifiers['optionDown']:
            self.color = 0, 0, 1, 0.5
        # > otherwise, color is red
        else:
            self.color = 1, 0, 0, 0.5

        # tell the glyph view to update
        self.refreshView()

    def draw(self, scale):

        if self.position is not None:
            # calculate shape position from center
            x = self.position.x - self.size*0.5
            y = self.position.y - self.size*0.5

            # set shape color
            fill(*self.color)
            stroke(None)

            # draw shape
            self.shape(x, y, self.size, self.size)

    def getToolbarTip(self):
        return "My Tool Tip"

# activate/deactivate the tool
if True:
    installTool(MyTool())
else:
    uninstallTool(MyTool())

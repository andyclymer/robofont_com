'''
collect glyph infos and sort by different parameters
'''

# step 1: collect info from glyphs
# glyph name, glyph width, amount of contours, amount of points

f = CurrentFont()

glyphInfos = []
for g in f:
    glyphInfos.append((g.name, g.width, len(g), sum([len(c) for c in g])))

# step 2: sort glyph infos by parameter

from operator import itemgetter

# sort by name (alphabetically)
print('first glyphs (alphabetically):')
for g in sorted(glyphInfos, key=itemgetter(0))[:20]:
    print(g[0], end=" ")
print()
print()

# sort by glyph width, widest first
print('widest glyphs:')
for g in sorted(glyphInfos, key=itemgetter(1), reverse=True)[:20]:
    print(g[0], end=" ")
print()
print()

# sort by amount of points
print('glyphs with most points:')
for g in sorted(glyphInfos, key=itemgetter(-1), reverse=True)[:20]:
    print(g[0], end=" ")
print()
print()



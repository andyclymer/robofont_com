import os
from vanilla.dialogs import getFolder

# select folder
folder = getFolder('Select a folder with UFOs')[0]

# make a subfolder for otfs
otfsFolder = os.path.join(folder, 'otfs')
os.mkdir(otfsFolder)

# iterate over all files in folder
for f in os.listdir(folder):

    # if file has .ufo extension
    if os.path.splitext(f)[-1] == '.ufo':

        # get full ufo path
        ufoPath = os.path.join(folder, f)

        # open ufo (without UI)
        font = OpenFont(ufoPath, showUI=False) # RF 1.8
        # font = OpenFont(ufoPath, showInterface=False) # RF 3.0

        # make otf path
        otfPath = os.path.join(folder, f.replace('.ufo', '.otf'))

        # generate otf font
        font.generate(path=otfPath, format='otf', decompose=True, checkOutlines=True)

        # close the ufo
        font.close()


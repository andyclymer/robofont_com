import os
from drawBot import *

# draw something
newDrawing()
size('A4Landscape')
fill(1, 0, 0)
fontSize(120)
rotate(5)
text('hello world!', (100, 200))

# make image path
folder = os.path.expanduser('~/Desktop')
imgPath = os.path.join(folder, 'hello.pdf')

# save image
saveImage(imgPath)

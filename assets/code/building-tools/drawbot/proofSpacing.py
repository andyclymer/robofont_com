from string import *

f = CurrentFont()
fontName = '%s-%s' % (f.info.familyName, f.info.styleName)

# install the font locally
# f.testInstall()

margin = 20
fontSize_ = 13
lineHeight_ = 1.1 * fontSize_

for group in [lowercase, uppercase, digits, punctuation]:

    txt = ''
    for char in list(group):
        # punctuation
        if group.startswith('!'):
            txt += '%s%s%s\n' % (char, char.join(list(lowercase)), char)
        # digits
        elif group.startswith('0'):
            txt += '%s%s%s\n' % (char, str(char).join(list(group)), char)
            txt += '%s%s%s\n' % (char, str(char*2).join(list(group)), char)
        # uppercase + lowercase
        else:
            txt += '%s%s%s\n' % (char, char.join(list(group)), char)

    # create new page
    newPage('A4Landscape')

    # set font
    if fontName in installedFonts():
        font(fontName)
    else:
        print('font %s not installed' % fontName)

    # calculate pos/size
    x = y = margin
    w = width() - margin*2
    h = height() - margin*2

    # set text sample
    fontSize(fontSize_)
    lineHeight(lineHeight_)
    textBox(txt, (x, y, w, h))

'''Glyph Proofer'''

# --------
# settings
# --------

f = CurrentFont()

# scale
s = 0.5

# glyph selection
i = 96 # CurrentGlyph() # (2, 4) # None

# canvas size
size('A4')
canvasWidth, canvasHeight = width(), height()
newDrawing()

# collect guides y
guidesY = {
    0,
    f.info.descender,
    f.info.xHeight,
    f.info.capHeight,
    f.info.ascender,
}

# -----
# setup
# -----

# get box y

boxHeight = (max(guidesY) - min(guidesY)) * s
boxY = (canvasHeight - boxHeight) * 0.5

# get glyph name(s)

if isinstance(i, RGlyph):
    glyphNames = [i.name]

elif type(i) is int:
    glyphNames = f.glyphOrder[i:i+1]

elif type(i) is tuple and len(i) == 2:
    glyphNames = f.glyphOrder[i[0]:i[1]+1]

else:
    glyphNames = f.glyphOrder

# draw glyphs

for glyphName in glyphNames:

    # get glyph
    g = f[glyphName]
    boxWidth = g.width*s

    # make new page
    newPage(canvasWidth, canvasHeight)

    # calculate position
    x = (canvasWidth - boxWidth) * 0.5
    y = boxY + abs(f.info.descender)*s

    # collect guides x
    guidesX = {x, x + boxWidth}

    # --------
    # draw box
    # --------

    save()
    fill(0.9)
    rect(x, boxY, boxWidth, boxHeight)
    restore()

    # -----------
    # draw guides
    # -----------

    save()
    lineDash(6, 3)
    stroke(0.5)

    # draw guides x
    for guideX in guidesX:
        line((guideX, 0), (guideX, height()))

    # draw guides y
    for guideY in guidesY:
        guideY = y + guideY*s
        line((0, guideY), (width(), guideY))
    restore()

    # ----------
    # draw glyph
    # ----------

    save()
    stroke(None)
    fill(0)
    translate(x, y)
    scale(s)
    drawGlyph(g)
    restore()

    # ------------
    # draw anchors
    # ------------

    r = 10
    save()
    fill(None)
    stroke(1, 0, 0)
    translate(x, y)
    for a in g.anchors:
        aX, aY = a.position
        line((aX*s-r, aY*s), (aX*s+r, aY*s))
        line((aX*s, aY*s-r), (aX*s, aY*s+r))
    restore()

    # ------------
    # draw caption
    # ------------

    t = 13 # em size
    captionX = t
    captionW = width() - t*2
    captionH = t*2

    save()
    font('Menlo-Bold')
    fontSize(t)
    fill(1, 0, 0)

    # top
    captionY = height() - t*3
    captionBox = captionX, captionY, captionW, captionH
    textBox(g.name, captionBox, align='left')
    if g.unicode:
        textBox(str(hex(g.unicode)), captionBox, align='right')

    # bottom
    captionY = 0
    captionBox = captionX, captionY, captionW, captionH
    textBox('%.2f' % g.width, captionBox, align='center')
    textBox('%.2f' % g.leftMargin, captionBox, align='left')
    textBox('%.2f' % g.rightMargin, captionBox, align='right')

    restore()

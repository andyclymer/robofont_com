from string import *

f = CurrentFont()
fontName = '%s-%s' % (f.info.familyName, f.info.styleName)
fontName = fontName.replace(' ', '')
print(fontName)

# install the font locally
# f.testInstall()

# check if font is installed
if fontName not in installedFonts():
    print('font %s not installed' % fontName)

# list OpenType features in font
features = listOpenTypeFeatures(fontName=fontName)
print(features)

# make sample
newPage('A4Landscape')
margin = 60
w = width() - margin*2
h = height() - margin*2

T = FormattedString()
T.font(fontName)
T.fontSize(24)

# turn features on/off
T.openTypeFeatures(liga=True)
T.openTypeFeatures(dlig=True)

# draw text
T.append('''Placeat nobis nam doloribus facere nobis non. Libero qui molestiae incidunt omnis illo et sunt ullam. Expedita eaque nesciunt mollitia esse quia facere saepe modi. Dolor provident in dolorum. Dolor ea eum dolorum voluptas placeat sint. Ut odio tempore sunt accusamus excepturi amet voluptatem. Omnis ut debitis id qui omnis minima. Officia optio tempore doloribus unde nisi. Porro est expedita quos. Sit aperiam deleniti libero est saepe eum sit pariatur. Exercitationem ut sint ut et non. Eos neque ullam iusto qui excepturi dicta labore. Voluptas et aut et eius consectetur eum eum. Ex consequatur consequatur sed amet molestias magnam delectus nam. Adipisci dignissimos enim nobis blanditiis dolore et dolorum. Blanditiis dolorem voluptatem quasi aliquam impedit porro facere dolorum. Ut fuga earum incidunt praesentium recusandae nam optio est. Soluta velit dolore a rerum aperiam aspernatur occaecati sit. Vitae aperiam quia quidem id. Quia ea reiciendis ut impedit nulla vel qui enim. Id dicta occaecati accusamus consequatur. Quia in distinctio enim nobis.''')
textBox(T, (margin, margin, w, h))

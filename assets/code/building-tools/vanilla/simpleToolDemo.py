from vanilla import *

class ToolDemo(object):

    def __init__(self):
        # first, create a floating window
        # (different than Window, FloatingWindow stays of top of other windows)
        self.w = FloatingWindow((123, 70), "myTool")

        # second, define some variables to use in the UI layout
        x, y = 10, 10
        padding = 10
        buttonHeight = 20

        # add a button for printing the selected glyphs
        self.w.printButton = Button(
                (x, y, -padding, buttonHeight), # position & size
                "print", # button label
                callback=self.printGlyphsCallback # button callback
            )

        # update the y-position before placing the next button
        y += buttonHeight + padding

        # add another button for paiting the selected glyphs
        self.w.paintButton = Button(
                (x, y, -padding, buttonHeight),
                "paint",
                callback=self.paintGlyphsCallback
            )

        # done creating the dialog - open the window
        self.w.open()

    def printGlyphsCallback(self, sender):
        '''Print the names of all selected glyphs.'''

        # get the current font
        f = CurrentFont()

        # if there is no current font:
        if f is None:
            # print a message to the Output Window
            print('please open a font first!')
            # and exit the function (rest of the code will not be executed)
            return

        # if the program gets here: we have a font!

        # if no glyphs are selected:
        if not len(f.selection):
            # print a message
            print('please select one or more glyphs first!')
            # and exit the function
            return

        # if the program gets here: we have selected glyph(s)!

        # loop over all selected glyphs
        for glyphName in f.selection:
            # print glyph name
            print(glyphName)

        # done! (print a blank line)
        print()

    def paintGlyphsCallback(self, sender):
        '''Paint all selected glyphs.'''

        f = CurrentFont()

        if f is None:
            print('please open a font first!')
            return

        if not len(f.selection):
            print('please select one or more glyphs first!')
            return

        for glyphName in f.selection:
            # save undo state
            f[glyphName].prepareUndo('painting glyph')
            # paint glyph
            f[glyphName].markColor = 1, 0, 0, 0.35
            # end undo state
            f[glyphName].performUndo()

# open the dialog
ToolDemo()


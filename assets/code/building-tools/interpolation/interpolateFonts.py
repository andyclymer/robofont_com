# get fonts
font1 = OpenFont()
font2 = OpenFont()

# define interpolation factor
factor = 0.5

# make destination font
destination = NewFont()

# this interpolates the glyphs
destination.interpolate(factor, font1, font2)

# this interpolates the kerning
# comment this line out of you're just testing
destination.kerning.interpolate(font1.kerning, font2.kerning, factor)

# done!
destination.changed()
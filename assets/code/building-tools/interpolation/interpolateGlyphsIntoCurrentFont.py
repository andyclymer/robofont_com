# the font where the interpolated glyphs will be stored
f = CurrentFont()

# the two master fonts
f1 = AllFonts().getFontsByStyleName('Regular')[0]
f2 = AllFonts().getFontsByStyleName('Bold')[0]

# the interpolation factor
factor = 0.4

# a list of glyph names to be interpolated
glyphNames = ['A', 'B', 'C', 'a', 'b', 'c']

# iterate over the glyph names
for glyphName in glyphNames:

    # if this glyph is not available in one of the masters, skip it
    if not glyphName in f1:
        print('%s not in %s, skipping…', (glyphName, f1))
        continue
    if not glyphName in f2:
        print('%s not in %s, skipping…' % (glyphName, f1))
        continue

    # if the glyph does not exist in the destination font, create it
    if not glyphName in f:
        f.newGlyph(glyphName)

    # interpolate glyph
    print('interpolating %s…' % glyphName)
    f[glyphName].interpolate(factor, f1[glyphName], f2[glyphName])
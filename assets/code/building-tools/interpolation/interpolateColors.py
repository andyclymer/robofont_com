# define two rgb colors
c1 = 0.5, 0.0, 0.3
c2 = 1.0, 0.6, 0.1

# total number of steps
steps = 14

# initial position
x, y = 0, 0

# calculate size for steps
w = width() / steps
h = height()

# iterate over the total amount of steps
for i in range(steps):

    # calculate interpolation factor for this step
    factor = i * 1.0 / (steps - 1)

    # interpolate each rgb channel separately
    r = c1[0] + factor * (c2[0] - c1[0])
    g = c1[1] + factor * (c2[1] - c1[1])
    b = c1[2] + factor * (c2[2] - c1[2])

    # draw a rectangle with the interpolated color
    fill(r, g, b)
    stroke(r, g, b)
    rect(x, y, w, h)

    # increase x-position for the next step
    x += w

from random import random

f = CurrentFont()
condensedLight = f["a#condensed_light"]
wideLight = f["a#wide_light"]
wideBold = f["a#wide_bold"]

diff = wideLight - condensedLight

destination = f.newGlyph("a#deltaexperiment")
destination.clear()

x = wideBold + (condensedLight - wideLight) * random()

destination.appendGlyph(x)
destination.width = x.width

f.changed()
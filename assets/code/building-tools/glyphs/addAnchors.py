# get the current font
f = CurrentFont()

# define name and position of the anchor
anchorName = 'top'
y = 530

# iterate over the names of selected glyphs
for glyphName in f.selection:

    # get the glyph
    g = f[glyphName]

    # get the bounds
    # bounds is either a tuple of 4 values or `None` if there is not contour
    bounds = g.bounds

    # check if bounds is not None
    if bounds:
        # define x-position as the middle of the glyph's bounding box
        xMiddle = g.bounds[0] + (g.bounds[2] - g.bounds[0]) * 0.5

        # add anchor to glyph
        g.prepareUndo('add anchor')
        g.appendAnchor(anchorName, (xMiddle, y))
        g.performUndo()

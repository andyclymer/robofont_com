'''simple accent builder'''

# ---------
# functions
# ---------

def buildGlyph(font, glyphName, accentsDict, verbose=True, overwrite=True):
    '''Build accented glyph from components and anchors.'''

    # a color to mark new glyphs
    color = 0, 1, 1, 0.5

    # check if glyph is included in accents dict
    if not accentsDict.has_key(glyphName):
        if verbose:
            print('# no entry for %s' % glyphName)
        return

    # get base glyph and accents
    baseGlyph, accents = accentsDict[glyphName]

    # remove glyph if it already exists
    if glyphName in font.keys() and overwrite:
        font.removeGlyph(glyphName)

    # build accented glyph
    if not glyphName in font.keys():
        if verbose:
            print('building %s...' % glyphName)
        font.compileGlyph(glyphName, baseGlyph, accents)
        font[glyphName].mark = color
        font[glyphName].update()

# ------------------
# glyph construction
# ------------------

accentsDict = {
    # accented : [ base, ( (accent1, anchor1), (accent2, anchor2), ... ) ],
    'aogonek'  : [ 'a',  ( ('ogonek', 'bottom'), ) ],
    'agrave'   : [ 'a',  ( ('grave', 'top'), ) ],
    'ccedilla' : [ 'c',  ( ('cedilla', 'bottom'), ) ],
    # ...add more items here...
}

# ------------
# build glyphs
# ------------

f = CurrentFont()

# choose which glyphs should be built
glyphNames = accentsDict.keys() # font.selection

for glyphName in glyphNames:
    buildGlyph(f, glyphName, accentsDict)

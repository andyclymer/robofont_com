import os
from subprocess import Popen

# path to vfb2ufo on your machine
ufo2vfbLocation = "/usr/local/bin/vfb2ufo"

# path to folder with input VFBs
vfbsFolder = u"/myVFBsFolder"

# path to folder for output UFOs
ufosFolder = u"/myUFOsFolder"

# collect all VFBs in VFBs folder
vfbs = [f for f in os.listdir(vfbsFolder) if os.path.splitext(f)[-1] == '.vfb']

# batch convert VFBs to UFO
for vfb in vfbs:

    # make file paths
    vfbPath = os.path.join(vfbsFolder, vfb)
    ufoPath = os.path.join(ufosFolder, vfb.replace('.vfb', '.ufo'))

    # call the vfb2ufo program
    p = Popen([ufo2vfbLocation, vfbPath, ufoPath]) # "-64", etc
    p.wait()

    # add a note to the converted UFO
    f = OpenFont(ufoPath, showInterface=False)
    f.info.note = 'converted from vfb with vfb2ufo'
    f.close(save=True)

from fontPens.digestPointPen import DigestPointStructurePen

f = OpenFont()

myPen = DigestPointStructurePen()
f['period'].drawPoints(myPen)

print(myPen.getDigest())

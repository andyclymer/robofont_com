nameRecords = [
  {
    "nameID"     : 19, # openTypeNameSampleText
    "platformID" : 1,  # Mac
    "encodingID" : 0,  # Roman
    "languageID" : 1,  # French
    "string"     : 'Voix ambiguë d’un cœur qui au zéphyr préfère les jattes de kiwi ',
  },
  {
    "nameID"     : 19, # openTypeNameSampleText
    "platformID" : 3,  # Windows
    "encodingID" : 1,  # Unicode
    "languageID" : 1,  # French
    "string"     : 'Voix ambiguë d’un cœur qui au zéphyr préfère les jattes de kiwi ',
  },
  {
    "nameID"     : 19, # openTypeNameSampleText
    "platformID" : 1,  # Mac
    "encodingID" : 0,  # Roman
    "languageID" : 2,  # German
    "string"     : 'Victor jagt zwölf Boxkämpfer quer über den großen Sylter Deich.',
  },
  {
    "nameID"     : 19, # openTypeNameSampleText
    "platformID" : 3,  # Windows
    "encodingID" : 1,  # Unicode
    "languageID" : 2,  # German
    "string"     : 'Victor jagt zwölf Boxkämpfer quer über den großen Sylter Deich.',
  },
  {
    "nameID"     : 19, # openTypeNameSampleText
    "platformID" : 1,  # Mac
    "encodingID" : 0,  # Roman
    "languageID" : 6,  # Spanish
    "string"     : 'Benjamín pidió una bebida de kiwi y fresa; Noé, sin vergüenza, la más exquisita champaña del menú.',
  },
  {
    "nameID"     : 19, # openTypeNameSampleText
    "platformID" : 3,  # Windows
    "encodingID" : 1,  # Unicode
    "languageID" : 6,  # Spanish
    "string"     : 'Benjamín pidió una bebida de kiwi y fresa; Noé, sin vergüenza, la más exquisita champaña del menú.',
  },
]

f = CurrentFont()
f.info.openTypeNameRecords = nameRecords

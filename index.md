---
layout: default
title: RoboFont
hideTitle: true
excludeSearch: true
excludeNavigation: true
pageId: home-page
slideShows:
  intro:
    - image: workspace/window-modes_single.png
    - image: workspace/font-overview.png
    - image: workspace/glyph-editor.png
    - image: workspace/space-center.png
    - image: workspace/scripting-window.png
    - image: workspace/features-editor.png
    - image: workspace/preferences_glyph-view-appearance.png
  intro-extensions:
    - image: extensions/overview/outliner.png
    - image: extensions/overview/italic-bowtie.png
    - image: extensions/overview/word-o-mat.png
    - image: extensions/overview/ramsay-st.png
    - image: extensions/overview/glifViewer.png
    - image: extensions/overview/GlyphBrowser.png
    - image: extensions/overview/GlyphConstruction.png
    - image: extensions/overview/GlyphNanny.png
    - image: extensions/overview/OverlayUFOs.png
    - image: extensions/overview/Slanter.png
  autoPlay: true
  height: 600
---

<style>
/* dirty hack to reduce the height of the 2nd slideshow */
.slides:nth-of-type(2) {
  height: 420px;
}
</style>

{% include_relative download-raw.md %}

<br/>

<script src="{{ site.baseurl }}/js/news.js"></script>

<div class="posts news">
    <h1><a href="http://forum.robofont.com/category/16/announcements/">Announcements:</a></h1>
    <div id="news-titles"></div>
</div>

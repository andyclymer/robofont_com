---
layout: page
title: Drafts
excludeSearch: true
excludeNavigation: true
---

{% assign drafItems = "" | split:"|" %}
{% for item in site.documentation %}
    {% if item.draft %}
        {% assign drafItems = drafItems | push: item %}
    {% endif %}
{% assign drafItems = drafItems | sort:"title" %}
{% endfor %}

A list of draft pages, see {% internallink "how-tos/writing-how-tos" %}.

<ul>
{% for item in drafItems %}
<li>
    <a class="draft-source" href="{{ site.gitlabrepo }}{{ item.path }}">edit</a>    
    <a href="{{ site.baseurl }}{{ item.url }}">{{ item.title }}</a>
</li>
{% endfor %}
</ul>

require_relative "helpers"


module Jekyll
  class InteralLink < Liquid::Tag

    include HelpersTools

    def initialize(tag_name, text, tokens)
      super
      text = text.strip()
      parts = text.split(" ")
      link = parts[0]
      if link.include?("#")
        anchorParts = link.split("#")
        @rawRootURL = anchorParts[0] + "\""
        @anchor = "#" + anchorParts[1][0..-2]
      else
        @rawRootURL = link
        @anchor = ""
      end
      @text = nil
      args = parts[1..-1].join(" ")
      args = args.strip().split("=")
      if args[0] == "text"
        @text = args[1]
      end
    end

    def render(context)
      found = nil
      url = get_value(context, @rawRootURL)
      if url.nil?
        pinkWarning("Cannot parse '#{@rawRootURL}' in #{context.registers[:page].path}")
        return
      end
      page_hash(context).each do |key, value|
          if key.include? url
              found = value
            break
          end
        end
      baseurl = context.registers[:site].baseurl
      unless found.nil?
        if @text.nil?
          @text = found["title"]
        else
          @text = get_value(context, @text)
        end
        "[#{@text}](#{baseurl}#{found.url}#{@anchor})"
      else
         pinkWarning("Internal Link not found: '#{url}' in #{context.registers[:page].path}")
      end
    end

  end
end

Liquid::Template.register_tag('internallink', Jekyll::InteralLink)
---
layout: page
title: Technical Specification
menuOrder: 2
---

* Table of Contents
{:toc}

## Operating system

RoboFont requires macOS {{ site.data.versions.minimumSystem }}+, and is fully compatible with more recent versions of macOS.

> Downloads for macOS 10.6 — 10.9 can be found [here][Download History].
{: .seealso}

## Font generation

RoboFont uses the [Adobe FDK][AFDKO] internally to generate OpenType fonts. The latest release of RoboFont ({{ site.data.versions.roboFont }}) embeds version {{ site.data.versions.afdko }} of the FDK.

> Users can choose to use their locally installed version of the Adobe FDK instead of the embedded one. See {% internallink 'workspace/preferences-window/miscellaneous' %}.
{: .seealso }

## Supported formats

RoboFont 3 uses [UFO3] as its native font format, and read/writes [UFO2].

RoboFont can open OpenType fonts, with a few limitations (for example, OpenType features cannot be imported in editable format). WOFF fonts are also supported.

RoboFont can open FontLab’s `.vfb` files, provided the [vfb2ufo] library is installed.

> - {% internallink "documentation/how-tos/converting-from-opentype-to-ufo" %}
> - {% internallink "documentation/how-tos/converting-from-vfb-to-ufo" %}
{: .seealso }

## Scripting language

RoboFont 3 is written entirely in Python {{ site.data.versions.python }}, and uses it as its scripting language.

RoboFont comes with its own embedded Python interpreter, so you don’t need to install anything else. All modules from the [Python Standard Library] are also included.

[Python Standard Library]: http://docs.python.org/3/library/

## Scripting API

RoboFont 3 uses the [FontParts] API (with a few extensions) to communicate to font objects programmatically.

> - {% internallink "documentation/building-tools/toolkit/fontparts" %}
> - {% internallink "documentation/building-tools/toolkit/robofab-fontparts" %}
{: .seealso }

## Embedded libraries

**RoboFont comes with batteries included.**

The following libraries are embedded in RoboFont, and available out-of-the-box:

<table>
  <tr>
    <th width='40%'>embedded library</th>
    <th width='30%'>version</th>
    <th width='30%'>commit</th>
  </tr>
  <tr>
    <td>booleanOperations</td>
    <td>{{ site.data.versions.booleanOperations }}</td>
    <td></td>
  </tr>
  <tr>
    <td>compositor</td>
    <td>{{ site.data.versions.compositor }}</td>
    <td></td>
  </tr>
  <tr>
    <td>cu2qu</td>
    <td>{{ site.data.versions.cu2qu }}</td>
    <td></td>
  </tr>
  <tr>
    <td>defcon</td>
    <td>{{ site.data.versions.defcon }}</td>
    <td></td>
  </tr>
  <tr>
    <td>defconAppKit</td>
    <td>{{ site.data.versions.defconAppKit }}</td>
    <td></td>
  </tr>
  <tr>
    <td>dialogKit</td>
    <td>{{ site.data.versions.dialogKit }}</td>
    <td></td>
  </tr>
  <tr>
    <td>extractor</td>
    <td>{{ site.data.versions.extractor }}</td>
    <td></td>
  </tr>
  <tr>
    <td>feaPyFoFum</td>
    <td>{{ site.data.versions.feaPyFoFum }}</td>
    <td></td>
  </tr>
  <tr>
    <td>fontCompiler</td>
    <td>{{ site.data.versions.fontCompiler }}</td>
    <td></td>
  </tr>
  <tr>
    <td>fontMath</td>
    <td>{{ site.data.versions.fontMath }}</td>
    <td></td>
  </tr>
  <tr>
    <td>fontParts</td>
    <td>{{ site.data.versions.fontParts }}</td>
    <td></td>
  </tr>
  <tr>
    <td>fontPens</td>
    <td>{{ site.data.versions.fontPens }}</td>
    <td></td>
  </tr>
  <tr>
    <td>fontTools</td>
    <td>{{ site.data.versions.fontTools }}</td>
    <td></td>
  </tr>
  <tr>
    <td>glyphConstruction</td>
    <td>{{ site.data.versions.glyphConstruction }}</td>
    <td></td>
  </tr>
  <tr>
    <td>glyphNameFormatter</td>
    <td>{{ site.data.versions.glyphNameFormatter }}</td>
    <td></td>
  </tr>
  <tr>
    <td>mutatorMath</td>
    <td>{{ site.data.versions.mutatorMath }}</td>
    <td></td>
  </tr>
  <tr>
    <td>ufoLib</td>
    <td>{{ site.data.versions.ufoLib }}</td>
    <td></td>
  </tr>
  <tr>
    <td>ufo2fdk</td>
    <td>{{ site.data.versions.ufo2fdk }}</td>
    <td></td>
  </tr>
  <tr>
    <td>ufo2svg</td>
    <td>{{ site.data.versions.ufo2svg }}</td>
    <td></td>
  </tr>
  <tr>
    <td>ufoNormalizer</td>
    <td>{{ site.data.versions.ufonormalizer }}</td>
    <td></td>
  </tr>
  <tr>
    <td>woffTools</td>
    <td>{{ site.data.versions.woffTools }}</td>
    <td></td>
  </tr>
</table>

> - {% internallink "documentation/building-tools/toolkit/libraries#embedded-libraries" text="Embedded libraries" %}
> - [Using locally installed versions instead of embedded libraries](#)
{: .seealso }

[AFDKO]: http://adobe.com/devnet/opentype/afdko.html
[UFO2]: http://unifiedfontobject.org/versions/ufo2/
[UFO3]: http://unifiedfontobject.org/versions/ufo3/
[Python 3]: http://docs.python.org/3/whatsnew/3.0.html
[Download History]: {{ site.baseurl }}/download-history
[vfb2ufo]: http://blog.fontlab.com/font-utility/vfb2ufo/
[FontParts]: http://fontparts.readthedocs.io/
[fontTools]: #
[mutatorMath]: #
[glyphNameFormatter]: #
[glyphConstruction]: #
[feaPyFoFum]: #
[vanilla]: #

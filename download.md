---
layout: page
title: Download
hideTitle: true
menuOrder: 200
---

{% include_relative download-raw.md %}

## Resellers

Resellers discount are not available.
{: .red }

RoboFont licenses can be acquired directly from the developer. See {% internallink 'licensing' %}.
